//
//  CatalogCell.swift
//  FirstTestSwift
//
//  Created by Sidney Silva on 20/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit

class CatalogCell: UICollectionViewCell {
    
    var imageItem = UIImageView()
    var priceLabel = UILabel()
    var conditionsLabel = UILabel()
    var loadingView = UIActivityIndicatorView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(imageItem)
        self.addSubview(priceLabel)
        self.addSubview(conditionsLabel)
        self.positionateSubviews()
        
        self.loadingView.center = self.imageItem.center
        loadingView.hidesWhenStopped=true
        loadingView.startAnimating()
        
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func positionateSubviews(){
        
        imageItem.setTranslatesAutoresizingMaskIntoConstraints(false)
        priceLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        conditionsLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let views = ["imageItem":self.imageItem, "priceLabel":self.priceLabel, "conditionLabel":self.conditionsLabel]
 
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[imageItem]-5-|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-5-[imageItem]-5-[priceLabel(20)]-5-[conditionLabel(20)]-5-|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[priceLabel]-5-|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[conditionLabel]-5-|", options: NSLayoutFormatOptions(0), metrics: nil, views: views))
        
    }
    
    func refeshCell(urlString:String, withprice price:String, andConditions conditions:String){
        self.loadingView.startAnimating()
        SyncItems().loadImageFromURLString(urlString, completion: { (image) -> Void in
            self.imageItem.image=image
            self.loadingView.stopAnimating()
        })
        priceLabel.text = price
        conditionsLabel.text = conditions
    }
}
