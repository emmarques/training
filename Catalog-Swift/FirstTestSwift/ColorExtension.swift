//
//  ColorExtension.swift
//  FirstTestSwift
//
//  Created by Sidney Silva on 21/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit

extension UIColor {
   
    
    func colorFromHexadecimalString(colorString:String) -> UIColor{
        var cString:NSString = colorString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(1)
        }
        
        if (cString.length != 6) {
            return UIColor.grayColor()
        }
        
        var rString = cString.substringToIndex(2)
        var gString = (cString.substringFromIndex(2) as NSString).substringToIndex(2)
        var bString = (cString.substringFromIndex(4) as NSString).substringToIndex(2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        NSScanner.localizedScannerWithString(rString).scanHexInt(&r)
        NSScanner.localizedScannerWithString(gString).scanHexInt(&g)
        NSScanner.localizedScannerWithString(bString).scanHexInt(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    
    
    
    
}
