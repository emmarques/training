//
//  Constants.swift
//  FirstTestSwift
//
//  Created by Sidney Silva on 21/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit

class Constants: NSObject {
   
    struct baseConstants {
        static let BASECOLOR = "#d6c9c9"
        static let BASEBACKGROUD = "#FFFAE6"
        static let BASEBUTTONCOLOR = "#AAAAAA"
        static let BASEBARBACKGROUND = "#FFCC00"
        static let BASEFONTCOLOR = "#000000"
        static let BASEDDESCRIPTIONCOLOR = "#000000"
        static let BASEHIGHLIGHT = "#b67700"
        static let BASEDISCOUNTPRICECOLOR = "000000"
        static let BASETITLECOLOR = "000000"
        static let BASESELECTEDCOLOR = "000000"
        static let HOSTNAME = "http://private-efa435-catalog16.apiary-mock.com"
        static let NOMELOJA = "Nome da loja"
    }
    
}
