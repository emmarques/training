//
//  DescriptionCollectionViewCell.swift
//  FirstTestSwift
//
//  Created by Sidney Silva on 18/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit


class DescriptionCollectionViewCell: UICollectionViewCell {
    
    var descriptionLabel = UILabel()
    var markLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        descriptionLabel = UILabel(frame: CGRectMake(5, 5, self.frame.size.width, self.frame.size.height/2))
        self.addSubview(descriptionLabel)
        markLabel = UILabel(frame: CGRectMake(5, self.frame.size.height/2, self.frame.size.width, self.frame.size.height/2))
        self.addSubview(markLabel)
        
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
   
   func refreshCellWithDescription(description:String, andMark mark:String){
        self.descriptionLabel.text = description
        self.markLabel.text = mark
    }
}
