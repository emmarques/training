//
//  HomeCollectionView.swift
//  FirstTestSwift
//
//  Created by Sidney Silva on 21/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit

class HomeCollectionView: UICollectionView, UICollectionViewDataSource {
    
    var catalog = NSArray()
    
    init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout, sectorsCatalog catalog:NSArray?) {
        super.init(frame: frame, collectionViewLayout: layout)
        if catalog != nil{
        self.catalog = catalog!
        }
        self.dataSource = self
        self.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
   func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.catalog.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! UICollectionViewCell
        var sectorLabel = UILabel(frame:CGRectMake(0, 0, cell.frame.width, cell.frame.height)
        
        
        )
        sectorLabel.text = self.catalog[indexPath.item].objectForKey("nome") as? String
        cell.addSubview(sectorLabel)
        cell.layer.borderWidth=1
        return cell
    }
    
    func refreshArray (array:NSArray){
        self.catalog = array
        self.reloadData()
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
