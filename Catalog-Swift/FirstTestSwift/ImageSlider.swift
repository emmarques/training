//
//  ImageSlider.swift
//  FirstTestSwift
//
//  Created by Sidney Silva on 19/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit

class ImageSlider: UIView {
    
   internal var imagesArray = NSMutableArray()
   internal var centerImage = UIImageView()
   internal var leftImage = UIImageView()
   internal var rightImage = UIImageView()
   internal var animating = false
    var currentImage = 0
    
    init(frame: CGRect, withImages images:NSArray) {
        super.init(frame: frame)
        if images.count > 0 {
            self.imagesArray = NSMutableArray(array: images)
            self.centerImage.image = imagesArray[0] as? UIImage
            if imagesArray.count > 1 {
                self.rightImage.image = imagesArray[1] as? UIImage
            }
            
            centerImage.backgroundColor = UIColor.lightGrayColor()
            leftImage.backgroundColor = UIColor.greenColor()
            rightImage.backgroundColor = UIColor.blueColor()
            
            self.addSubview(centerImage)
            self.addSubview(rightImage)
            self.addSubview(leftImage)
            self.positionateImageViews(animated: false)
        }
        var panGesture = UIPanGestureRecognizer(target: self, action: "handleWithPan:")
        self.addGestureRecognizer(panGesture)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func handleWithPan(gesture:UIPanGestureRecognizer){
        var velocity = gesture.velocityInView(self)
        centerImage.center = CGPointMake(centerImage.center.x+(velocity.x/50), centerImage.center.y)
        rightImage.center = CGPointMake(rightImage.center.x+(velocity.x/50), rightImage.center.y)
        leftImage.center = CGPointMake(leftImage.center.x+(velocity.x/50), leftImage.center.y)
        
        if gesture.state == UIGestureRecognizerState.Ended {
        if velocity.x > 1000 && velocity.x > 0 {
            self.previous()
            return
        }
        else {
            if (velocity.x < -1000 ) && (velocity.x < 0){
                self.next()
                return
               
        }
            }
        if centerImage.center.x>self.frame.size.width && !animating{
            self.previous()
            return
        }else{
            if centerImage.center.x<0 && !animating{
                self.next()
                return
            }
            }
        self.positionateImageViews(animated: true)
        }
        
    }
    
    func addImages(image:UIImage){
        self.imagesArray.addObject(image)
    }
    
    func next(){
        NSLog("next")
        if rightImage.image != nil && !self.animating{
        UIView.animateWithDuration(0.2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options:UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
            self.animating = true
            self.centerImage.frame = CGRectMake(-self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)
            self.rightImage.frame = self.frame
            }, completion: {finish in
                self.leftImage.image = (self.imagesArray[self.currentImage] as! UIImage)
                self.currentImage = self.currentImage + 1
                self.centerImage.image = (self.imagesArray[self.currentImage] as! UIImage)
                
                if self.currentImage+1 < self.imagesArray.count{
                    self.rightImage.image = (self.imagesArray[self.currentImage+1] as! UIImage)
                } else {
                   self.rightImage.image=nil
                }
                 NSLog("CurrentImage: %i",self.currentImage)
                self.animating = false
                self.positionateImageViews(animated: false)
                self.setNeedsLayout()
        })
        } else {
             self.positionateImageViews(animated: true)
            NSLog("next just positionate view")
        }
    }
    
    func previous(){
        NSLog("previous")
     if leftImage.image != nil && !self.animating{
        UIView.animateWithDuration(0.2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options:UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
            self.animating = true
            self.centerImage.frame = CGRectMake(self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)
            self.leftImage.frame = self.frame
            }, completion: {finish in
                self.rightImage.image = (self.imagesArray[self.currentImage] as! UIImage)
                self.currentImage = self.currentImage - 1
                self.centerImage.image = (self.imagesArray[self.currentImage] as! UIImage)
                
                if self.currentImage-1 >= 0{
                    self.leftImage.image = (self.imagesArray[self.currentImage-1] as! UIImage)
                }else{
                    self.leftImage.image = nil
                }
                NSLog("CurrentImage: %i",self.currentImage)
                self.animating = false
                self.positionateImageViews(animated: false)
                self.setNeedsLayout()
            })

        }else {
            self.positionateImageViews(animated: true)
            NSLog("previous just positionate view")
        }
    }
    
    func positionateImageViews(animated animate:Bool){
        if !self.animating{
        if animate {
             NSLog("animating")
            self.animating = true
        UIView.animateWithDuration(0.2, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options:UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
            self.centerImage.frame = self.frame
            self.leftImage.frame = CGRectMake(-self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)
            self.rightImage.frame = CGRectMake(self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)
            }, completion: {finish in
                self.animating=false
        })    }
    
    else{
            self.centerImage.frame = self.frame
            self.leftImage.frame = CGRectMake(-self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)
            self.rightImage.frame = CGRectMake(self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)
            NSLog("positionateImageViews not animating")
    }
        }

    }
}
