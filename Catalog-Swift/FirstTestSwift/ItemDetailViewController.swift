//
//  ItemDetailViewController.swift
//  FirstTestSwift
//
//  Created by Sidney Silva on 18/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit

class ItemDetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        var flowlayout = UICollectionViewFlowLayout()
        flowlayout.estimatedItemSize = CGSizeMake(self.view.frame.size.width,75 )
        flowlayout.minimumInteritemSpacing = 5
        flowlayout.minimumLineSpacing = 5
        var collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout:flowlayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.registerClass(DescriptionCollectionViewCell.self, forCellWithReuseIdentifier: "description")
        self.view.addSubview(collectionView)
        collectionView.backgroundColor = UIColor.grayColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        //self.navigationController?.pushViewController(ItemsCatalogViewController(), animated: true)
    }
    
   func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if indexPath.item == 0 {
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! UICollectionViewCell
            var images = NSMutableArray()
            images.addObject(UIImage(named: "wishlist.png")!)
            images.addObject(UIImage(named: "wishlistSelected.png")!)
            images.addObject(UIImage(named: "house.png")!)
            var imagesliderView = ImageSlider(frame: cell.frame, withImages: images)
            cell.addSubview(imagesliderView)
            cell.layer.borderWidth = 1
            return cell
        }else{
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("description", forIndexPath: indexPath) as! DescriptionCollectionViewCell
            cell.refreshCellWithDescription(NSString(format: "celula %i", indexPath.item) as String, andMark: NSString(format: "marca %@", "banana") as String)
            cell.layer.borderWidth = 1
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
