//
//  ItemsCatalogViewController.swift
//  FirstTestSwift
//
//  Created by Sidney Silva on 18/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit

class ItemsCatalogViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "nome"
        var flowlayout = UICollectionViewFlowLayout()
        flowlayout.itemSize = CGSizeMake(self.view.frame.size.width/3-5,self.view.frame.size.width/3*1.5 )
        flowlayout.minimumInteritemSpacing = 5
        flowlayout.minimumLineSpacing = 5
        var collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout:flowlayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerClass(CatalogCell.self, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(collectionView)
        collectionView.backgroundColor = UIColor.grayColor()
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        self.navigationController?.pushViewController(ItemDetailViewController(), animated: true)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CatalogCell
        cell.layer.borderWidth = 1
        cell.refeshCell("http://phfwc.org/wp-content/uploads/2015/01/example.jpg", withprice: "R$ 13,99", andConditions: "3x R$ 5,00")
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 50;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
