//
//  SyncItems.swift
//  FirstTestSwift
//
//  Created by Sidney Silva on 20/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit

enum requestType: String {
    case requestTypePost = "POST"
    case requestTypeGet = "GET"
}


class SyncItems: NSObject {
    
        func loadImageFromURLString(urlString: String, completion: (result: UIImage) -> Void) {
        let url = NSURL(string: urlString)
        var request = NSURLRequest(URL: url!)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) { (response, data, error) -> Void in
            if (error == nil) {
                let image = UIImage(data: data)
                completion(result: image!)
                }
            }
        }
    
    func loadDictionaryWithRequestType(typeOfRequest:requestType, usingRequestString requestString:String, withDictionaryPost postDic:NSDictionary?, completition:(data:NSDictionary)->Void){
        self.loadDataWithRequestType(typeOfRequest, usingRequestString: requestString, withDictionaryPost: postDic) { (data) -> Void in
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            if err != nil {
                println("Error loadDictionaryWithRequestType: \(err)")
            }else{
                completition(data: json!)
            }
        }
        
    }
    
    func loadArrayWithRequestType(typeOfRequest:requestType, usingRequestString requestString:String, withDictionaryPost postDic:NSDictionary?, completition:(data:NSArray)->Void){
        self.loadDataWithRequestType(typeOfRequest, usingRequestString: requestString, withDictionaryPost: postDic) { (data) -> Void in
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSArray
            if err != nil {
                println("Error loadArrayWithRequestType: \(err)")
            }else{
                completition(data: json!)
            }
        }
      
    }
    
   private func loadDataWithRequestType(typeOfRequest:requestType, usingRequestString requestString:String, withDictionaryPost postDic:NSDictionary?, completition:(data:NSData)->Void) {
        var mutableRequest = NSMutableURLRequest(URL: NSURL(string: "\(Constants.baseConstants.HOSTNAME)\(requestString)")!)
        var session = NSURLSession.sharedSession()
        mutableRequest.HTTPMethod = typeOfRequest.rawValue

        if (postDic != nil) {
            var err: NSError?
            mutableRequest.HTTPBody = NSJSONSerialization.dataWithJSONObject(postDic!, options: nil, error: &err)
            mutableRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            mutableRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        }
            var task = session.dataTaskWithRequest(mutableRequest, completionHandler: {data, response, error -> Void in          
                if error != nil {
                    NSLog("Error loadDataWithRequestType: \(error)")
                }else{
                    completition (data: data)
                }
            })
            
            task.resume()
        }
    }
