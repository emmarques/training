//
//  ViewController.swift
//  FirstTestSwift
//
//  Created by Sidney Silva on 18/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var catalog: NSArray = []
    var collectionViewSectors: HomeCollectionView!
    var collectionMenuView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor().colorFromHexadecimalString(Constants.baseConstants.BASEBACKGROUD)
        self.title = Constants.baseConstants.NOMELOJA
        var flowlayout = UICollectionViewFlowLayout()
        flowlayout.estimatedItemSize = CGSizeMake(self.view.frame.size.width/2-5,40 )
        flowlayout.minimumInteritemSpacing = 5
        flowlayout.minimumLineSpacing = 5
        flowlayout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        collectionMenuView = UICollectionView(frame: CGRectMake(0, 0, self.view.frame.width, 50), collectionViewLayout:flowlayout)
        collectionMenuView.delegate = self
        collectionMenuView.dataSource = self
        collectionMenuView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionMenuView.backgroundColor = UIColor().colorFromHexadecimalString(Constants.baseConstants.BASEBARBACKGROUND)
        self.view.addSubview(collectionMenuView)
        collectionMenuView.backgroundColor = UIColor.grayColor()
        // Do any additional setup after loading the view, typically from a nib.
        
       
        
        var flowlayoutSectors = UICollectionViewFlowLayout()
        flowlayoutSectors.estimatedItemSize = CGSizeMake(self.view.frame.size.width/2-5,40 )
        flowlayoutSectors.minimumInteritemSpacing = 5
        flowlayoutSectors.minimumLineSpacing = 5
        
        self.collectionViewSectors = HomeCollectionView(frame: CGRectMake(0,self.collectionMenuView.frame.size.height, self.view.frame.size.width, self.view.frame.height-collectionMenuView.frame.height), collectionViewLayout: flowlayoutSectors, sectorsCatalog: nil)
        self.collectionViewSectors.delegate=self;
        self.view.addSubview(collectionViewSectors)
        self.collectionViewSectors.backgroundColor = UIColor().colorFromHexadecimalString(Constants.baseConstants.BASEBACKGROUD)
        
        SyncItems().loadArrayWithRequestType(requestType.requestTypeGet, usingRequestString: "/categorias", withDictionaryPost: nil) { (array) -> Void in
            self.catalog = array
            self.collectionViewSectors.refreshArray(self.catalog[0].objectForKey("subCategoria") as! NSArray)
            self.collectionMenuView.reloadData()
            //self.collectionView(self.collectionMenuView, didSelectItemAtIndexPath: NSIndexPath(forItem: 0, inSection: 0))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        if collectionView.isEqual(self.collectionMenuView){
            self.collectionViewSectors.refreshArray((self.catalog[indexPath.item].objectForKey("subCategoria") as! NSArray))
        } else {
            self.navigationController?.pushViewController(ItemsCatalogViewController(), animated: true)
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! UICollectionViewCell
        cell.layer.borderWidth = 1
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catalog.count;
    }

}

