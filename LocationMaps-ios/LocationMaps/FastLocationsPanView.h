//
//  FastLocationsPanView.h
//  LocationMaps
//
//  Created by Sidney Silva on 20/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FastLocationsPanView : UIView <UIGestureRecognizerDelegate>
- (instancetype)initWithFrame:(CGRect)frame;
@end
