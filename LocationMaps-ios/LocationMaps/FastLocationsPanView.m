//
//  FastLocationsPanView.m
//  LocationMaps
//
//  Created by Sidney Silva on 20/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "FastLocationsPanView.h"
@interface FastLocationsPanView()
@property UIButton *fastButton1;
@property UIButton *fastButton2;
@property UIButton *fastButton3;
@property UIButton *fastButton4;
@property CGRect initialFrame;
@end

@implementation FastLocationsPanView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.initialFrame = frame;
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
        [panGesture setDelegate:self];
        [self addGestureRecognizer:panGesture];
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    }
    return self;
}

- (CGPoint)translationInView:(UIView *)view{
    return CGPointMake(50, 0);
}


-(void)handlePan: (UIPanGestureRecognizer *)panGesture{
        
        CGPoint translation = [panGesture translationInView:self];
     NSLog(@"translation =%f",panGesture.view.center.x+translation.x);
    if ((panGesture.view.center.x+translation.x> self.initialFrame.origin.x-25)&&(panGesture.view.center.x+translation.x< self.window.bounds.size.width+15)) {
       
        panGesture.view.center = CGPointMake(panGesture.view.center.x + translation.x,
                                             panGesture.view.center.y);
        [panGesture setTranslation:CGPointMake(0, 0) inView:self];
    }
    
    if (panGesture.state==UIGestureRecognizerStateEnded) {
        NSLog(@"end");
    }
}

//-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
//    float move;
//    CGPoint nowPoint = [[touches anyObject] locationInView:self];
//    CGPoint prevPoint = [[touches anyObject] previousLocationInView:self];
//    move += prevPoint.x - nowPoint.x;
//    if (self.frame.origin.x-move >= self.initialFrame.origin.x-50) {
//        self.frame = CGRectMake(self.frame.origin.x-move, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
//    }
//}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
