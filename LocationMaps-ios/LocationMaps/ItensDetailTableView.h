//
//  ItensDetailTableView.h
//  LocationMaps
//
//  Created by Sidney Silva on 20/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, tableViewDetailLevel) {
    tableViewDetailLevelShort,
    tableViewDetailLevelLong,
};

@interface ItensDetailTableView : UITableView <UITableViewDelegate,UITableViewDataSource>
-(void)updateData:(NSArray*)information;
- (instancetype)initWithFrame:(CGRect)frame andDetailLevel:(tableViewDetailLevel) detailLevel withInformation:(NSArray *)information;
@end
