//
//  ItensDetailTableView.m
//  LocationMaps
//
//  Created by Sidney Silva on 20/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "ItensDetailTableView.h"
#import "LongDetailTableViewCell.h"
#import "ShortDetailTableViewCell.h"

@interface ItensDetailTableView()
@property tableViewDetailLevel detailLevel;
@property NSArray *information;
@end

@implementation ItensDetailTableView

- (instancetype)initWithFrame:(CGRect)frame andDetailLevel:(tableViewDetailLevel) detailLevel withInformation:(NSArray *)information
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor= [UIColor clearColor];
        _detailLevel = detailLevel;
        [self setDataSource:self];
        [self setDelegate:self];
        self.information = information;
        [self registerClass:[LongDetailTableViewCell class] forCellReuseIdentifier:@"longCell"];
        [self registerClass:[ShortDetailTableViewCell class] forCellReuseIdentifier:@"shortCell"];
        self.estimatedRowHeight = 44.0;
        self.rowHeight = UITableViewAutomaticDimension;
        self.separatorColor = [UIColor clearColor];
    }
    return self;
}

-(void)updateData:(NSArray*)information{
    self.information = information;
    [self reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.information count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.detailLevel == tableViewDetailLevelLong) {
        LongDetailTableViewCell *cell = [self dequeueReusableCellWithIdentifier:@"longCell" forIndexPath:indexPath];
        [cell updateCell:[self.information objectAtIndex:indexPath.row]];
        return cell;
    }else{
        ShortDetailTableViewCell *cell = [self dequeueReusableCellWithIdentifier:@"shortCell" forIndexPath:indexPath];
         [cell updateCell:[self.information objectAtIndex:indexPath.row]];
        return cell;
    }
}

@end
