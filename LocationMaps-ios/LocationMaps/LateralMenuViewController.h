//
//  LateralMenuViewController.h
//  LocationMaps
//
//  Created by Sidney Silva on 20/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LateralMenuViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@end
