//
//  ListOfPlacesViewController.m
//  LocationMaps
//
//  Created by Sidney Silva on 20/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "ListOfPlacesViewController.h"
#import "ItensDetailTableView.h"
#import "MapSearch.h"
#import <MapKit/MapKit.h>
#import "MapViewController.h"
@interface ListOfPlacesViewController ()
@property (strong)UISearchBar *searchBar;
@property (strong)ItensDetailTableView *detailTableView;
@property (strong)NSArray* places;
@end

@implementation ListOfPlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *mapViewButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(loadMapViewController)];
    [self.navigationItem setRightBarButtonItem:mapViewButton animated:YES];
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(50, 0, self.view.frame.size.width-100, self.navigationController.navigationBar.frame.size.height)];
    _searchBar.showsCancelButton = NO;
    _searchBar.delegate = self;
    [self.navigationController.navigationBar addSubview:_searchBar];
    
    _detailTableView = [[ItensDetailTableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andDetailLevel:tableViewDetailLevelLong withInformation:nil];
    [self.view addSubview:_detailTableView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    //40.719812, -74.000359
    MapSearch *mapSearch = [MapSearch new];
   [mapSearch issueLocalSearchLookup:searchBar.text completionHandler:^(NSArray *result) {
        [_detailTableView updateData:result];
       self.places = result;
    }];
    
   
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

-(void)loadMapViewController{
    MapViewController *mapView;
    if (self.places)
        mapView = [[MapViewController alloc]initWithPlaces:self.places];
    else
        mapView = [MapViewController new];
    
    [self.navigationController pushViewController:mapView animated:YES];
    [self.searchBar resignFirstResponder];
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
