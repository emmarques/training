//
//  LongDetailTableViewCell.m
//  LocationMaps
//
//  Created by Sidney Silva on 20/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "LongDetailTableViewCell.h"
#import <MapKit/MapKit.h>
@interface LongDetailTableViewCell()
@property UILabel *nameLabel;
@property UILabel *addressLabel;
@property UILabel *gradeLabel;
@property UIView *priceView;
@property UILabel *distanceLabel;
@end
@implementation LongDetailTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _nameLabel = [UILabel new];
        _nameLabel.translatesAutoresizingMaskIntoConstraints =NO;
        _nameLabel.layer.borderWidth = 1;
        [self addSubview:_nameLabel];
        
        _addressLabel = [UILabel new];
        _addressLabel.translatesAutoresizingMaskIntoConstraints= NO;
        [self addSubview:_addressLabel];
        
        _gradeLabel = [UILabel new];
        _gradeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _gradeLabel.textAlignment =NSTextAlignmentRight;
        [self addSubview:_gradeLabel];
        
        _distanceLabel = [UILabel new];
        _distanceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _distanceLabel.textAlignment =NSTextAlignmentRight;
        [self addSubview:_distanceLabel];
        
        _priceView = [UIView new];
        _priceView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_priceView];
        
        self.imageView.backgroundColor = [UIColor blueColor];
        
        self.layer.cornerRadius = 5;
        [self setPositionsOfViews];
    }
    return self;
}

-(void)updateCell:(MKMapItem*)information{
    _nameLabel.text = @"bla bla";
    _addressLabel.text = @"local place bla bla";
    _gradeLabel.text = @"9.2";
    _distanceLabel.text = @"0.2km";
    _priceView.backgroundColor= [UIColor greenColor];
    [self sizeToFit];
}


-(void)setPositionsOfViews{
    NSDictionary *views =@{@"name":_nameLabel,
                           @"address":_addressLabel,
                           @"grade":_gradeLabel,
                           @"distance":_distanceLabel,
                           @"price":_priceView};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[name]-5-|" options:0 metrics:nil views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[address]-0-[distance(50)]-5-|" options:0 metrics:nil views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[price(100)]-0-[grade]-5-|" options:0 metrics:nil views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[name(20)]-0-[address(15)]-0-[price(20)]-5-|" options:NSLayoutFormatAlignAllLeft metrics:nil views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-25-[distance(15)]-0-[grade(20)]-5-|" options:0 metrics:nil views:views]];
    
    
}

@end
