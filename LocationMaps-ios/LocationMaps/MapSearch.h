//
//  MapSearch.h
//  LocationMaps
//
//  Created by Sidney Silva on 20/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapSearch : NSObject
- (void)issueLocalSearchLookup:(NSString *)searchString completionHandler:(void(^)(NSArray* result))completion;
@end
