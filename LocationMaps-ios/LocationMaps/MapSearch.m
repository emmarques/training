//
//  MapSearch.m
//  LocationMaps
//
//  Created by Sidney Silva on 20/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "MapSearch.h"
#import <MapKit/MapKit.h>

typedef void(^completionHandler)(NSArray*, NSError *error);

@interface MapSearch()
@property (nonatomic, strong) MKLocalSearch *localSearch;
@property (nonatomic, strong) MKLocalSearchRequest *localSearchRequest;
@property CLLocationCoordinate2D coords;
@property CLLocationManager *currentLocation;
@end
@implementation MapSearch


- (void)issueLocalSearchLookup:(NSString *)searchString completionHandler:(void(^)(NSArray* result))completion{
    
    if ([self.currentLocation respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.currentLocation requestWhenInUseAuthorization];
    }
    [self.currentLocation startUpdatingLocation];
   // NSLog(@"%@", _currentLocation);
    self.coords = _currentLocation.location.coordinate;
    if (!_currentLocation.location) {
        double latitude =-26.907812;
        double longitude =-49.07981;
        self.coords = CLLocationCoordinate2DMake(latitude,longitude);
    }
    // Set the size (local/span) of the region (address, w/e) we want to get search results for.
    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = MKCoordinateRegionMake(self.coords, span);
    
    // Create the search request
    self.localSearchRequest = [[MKLocalSearchRequest alloc] init];
    self.localSearchRequest.region = region;
    self.localSearchRequest.naturalLanguageQuery = searchString;
    
    // Perform the search request...
    self.localSearch = [[MKLocalSearch alloc] initWithRequest:self.localSearchRequest];
    [self.localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        
        if(error){
            
            NSLog(@"localSearch startWithCompletionHandlerFailed!  Error: %@", error);
            return;
            
        } else {
            completion(response.mapItems);

        }
    }];
    
}


@end
