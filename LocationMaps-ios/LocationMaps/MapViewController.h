//
//  ViewController.h
//  LocationMaps
//
//  Created by Sidney Silva on 17/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface MapViewController : UIViewController <MKMapViewDelegate,UISearchBarDelegate>
- (instancetype)initWithPlaces:(NSArray *)places;
@end

