//
//  ViewController.m
//  LocationMaps
//
//  Created by Sidney Silva on 17/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>
#import "FastLocationsPanView.h"
#import "ItensDetailTableView.h"
#import "MapSearch.h"
#import "PlaceDetailViewController.h"
@interface MapViewController ()
// Don't forget to hook this up through IB/NiB - You'll also need to set the mapview's delegate to be this object as well...
@property (nonatomic, strong) MKMapView *mapView;

// LocalSearch Stuff...

@property CLLocationCoordinate2D coords;
@property CLLocationManager *currentLocation;
@property UISearchBar *searchBar;
@property ItensDetailTableView *detailTableView;
@property NSArray *mapItems;
@end

@implementation MapViewController

- (instancetype)initWithPlaces:(NSArray *)places
{
    self = [super init];
    if (self) {
        self.mapView = [[MKMapView alloc]init];
        [self populateMapWithPlaces:places];
    }
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(50, 0, self.view.frame.size.width-100, self.navigationController.navigationBar.frame.size.height)];
    _searchBar.showsCancelButton = NO;
    _searchBar.delegate = self;
    [self.navigationController.navigationBar addSubview:_searchBar];
    
    UIBarButtonItem *mapViewButton = [UIBarButtonItem new];
    mapViewButton.title = @"L";
    mapViewButton.target=self;
    self.navigationController.navigationItem.rightBarButtonItem = mapViewButton;
    
    if (!self.mapView) {
        self.mapView = [[MKMapView alloc]init];
    }
    
    self.mapView.frame =CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height);

    [self.mapView setDelegate:self];
    [self.view addSubview:self.mapView];
    [self.mapView showsUserLocation];
    
    FastLocationsPanView *panView = [[FastLocationsPanView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-10, 50, 50, 200)];
    [self.mapView addSubview:panView];
    
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self.mapView setMapType:MKMapTypeHybrid];
    [self.mapView setMapType:MKMapTypeStandard];
}

// Ex: [self issueLocalSearchLookup:@"grocery"];



#pragma mark - searchBar

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    MapSearch *mapSearch = [MapSearch new];
    [mapSearch issueLocalSearchLookup:searchBar.text completionHandler:^(NSArray *result) {
        [self populateMapWithPlaces:result];
    }];
    
}

-(void)populateMapWithPlaces:(NSArray *)places{
    [self.mapView removeAnnotations:self.mapView.annotations];
    for(MKMapItem *mapItem in places){
        // Show pins, pix, w/e...
        MKPointAnnotation *point = [[MKPointAnnotation alloc]init];
        [point setTitle:mapItem.name];
        [point setCoordinate:mapItem.placemark.coordinate];
        [point setSubtitle:mapItem.phoneNumber];
        [self.mapView addAnnotation:point];
    }
    MKCoordinateSpan span = MKCoordinateSpanMake(0.05, 0.05);
    MKMapItem *mapItem = [places objectAtIndex:0];
    MKCoordinateRegion region = MKCoordinateRegionMake(mapItem.placemark.coordinate, span);
    [self.mapView setRegion:region animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annotationView = nil;
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        annotationView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"Pin"];
        if (annotationView == nil)
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Pin"];
            annotationView.canShowCallout = YES;
           // annotationView.animatesDrop = YES;
            annotationView.image = [UIImage imageNamed:@"pin.png"];
            
            UIButton *buttonCallOut = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            annotationView.rightCalloutAccessoryView = buttonCallOut;

        }
    }
    return annotationView;
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    id <MKAnnotation> annotation = [view annotation];
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        //TODO: chamar a nova viewController
        [self.navigationController pushViewController:[PlaceDetailViewController new] animated:YES];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

#pragma mark - UIbarbutton actions

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [self.searchBar resignFirstResponder];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [self.searchBar removeFromSuperview];
}

@end
