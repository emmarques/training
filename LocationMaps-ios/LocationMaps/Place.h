//
//  Place.h
//  LocationMaps
//
//  Created by Sidney Silva on 22/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Place : NSObject
@property NSString *name;
@property NSString *phone;
@property NSString *address;
@property NSString *placeDescription;
@property NSArray  *tips;
@property NSArray  *photos;
@property float    *gradeOfPlace;
@property NSNumber *chekIns;
@end
