//
//  PlaceDetailViewController.h
//  LocationMaps
//
//  Created by Sidney Silva on 22/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceDetailViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@end
