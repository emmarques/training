//
//  PlaceDetailViewController.m
//  LocationMaps
//
//  Created by Sidney Silva on 22/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "PlaceDetailViewController.h"
#import <MapKit/MapKit.h>
#import "Place.h"
@interface PlaceDetailViewController ()
@property Place *place;
@end

@implementation PlaceDetailViewController


- (instancetype)initWithPlace:(Place*)place
{
    self = [super init];
    if (self) {
        self.place = place;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UITableView *tableView = [[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    [self.view addSubview:tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
