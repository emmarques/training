//
//  ShortDetailTableViewCell.m
//  LocationMaps
//
//  Created by Sidney Silva on 20/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "ShortDetailTableViewCell.h"
#import <MapKit/MapKit.h>
@interface ShortDetailTableViewCell()
@end
@implementation ShortDetailTableViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateCell:(NSArray *)information{
    self.textLabel.text = @"bla bla";
    self.detailTextLabel.text = @"detail bla bla";
    self.imageView.backgroundColor = [UIColor blueColor];
}

-(void)positionateViews{
}
@end
