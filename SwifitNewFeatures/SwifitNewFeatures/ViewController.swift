//
//  ViewController.swift
//  SwifitNewFeatures
//
//  Created by Sidney Silva on 22/05/15.
//  Copyright (c) 2015 ToolSystems. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    struct Resolution {
        var width = 0
        var height = 0
    }
    class VideoMode {
        var resolution = Resolution()
        var interlaced = false
        var frameRate = 0.0
        var name: String?
    }
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        var resolution = Resolution()
        resolution.width=1920
        resolution.height=1080
        
        var cinema = VideoMode()
        cinema.resolution=resolution
        
        
        
        
        let statistics = calculateStatistics(scores: [5, 3, 100, 1, 9])
        println(statistics.sum)
        println(statistics.0)
        
        var greeting = "bom dia"
        greeting.insert("!", atIndex: greeting.endIndex.predecessor())
        println(greeting)
        
        var threeDoubles = [Double](count: 3, repeatedValue: 0.0)
        println(threeDoubles)
        
        var anotherThreeDoubles = [Double](count: 3, repeatedValue: 2.5)
        println(anotherThreeDoubles)
        
        var sixDoubles = threeDoubles + anotherThreeDoubles
        println(sixDoubles)
        
        var shoppingList = ["Eggs", "Milk"]
        shoppingList.append("Flour")
        shoppingList += ["Baking Powder"]
        shoppingList += ["Chocolate Spread", "Cheese", "Butter"]
        println(shoppingList)
        shoppingList[4...6] = ["Bananas", "Apples"]
        println(shoppingList)
        
        
        let oddDigits: Set = [1, 3, 5, 7, 9]
        let evenDigits: Set = [0, 2, 4, 6, 8]
        let singleDigitPrimeNumbers: Set = [2, 3, 5, 7]
        
        sorted(oddDigits.union(evenDigits))
        // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        sorted(oddDigits.intersect(evenDigits))
        // []
        sorted(oddDigits.subtract(singleDigitPrimeNumbers))
        // [1, 9]
        sorted(oddDigits.exclusiveOr(singleDigitPrimeNumbers))
        // [1, 2, 9]

        
        let houseAnimals: Set = ["🐶", "🐱"]
        let farmAnimals: Set = ["🐮", "🐔", "🐑", "🐶", "🐱"]
        let cityAnimals: Set = ["🐦", "🐭"]
        
        houseAnimals.isSubsetOf(farmAnimals)
        // true
        farmAnimals.isSupersetOf(houseAnimals)
        // true
        farmAnimals.isDisjointWith(cityAnimals)
        // true

        
        let anotherPoint = (2, 0)
        switch anotherPoint {
        case (let x, 0):
            println("on the x-axis with an x value of \(x)")
        case (0, let y):
            println("on the y-axis with a y value of \(y)")
        case let (x, y):
            println("somewhere else at (\(x), \(y))")
        }
        
        
        let integerToDescribe = 5
        var description = "The number \(integerToDescribe) is"
        switch integerToDescribe {
        case 2, 3, 5, 7, 11, 13, 17, 19:
            description += " a prime number"
            fallthrough
        case 5:
            description += ", and also"
            fallthrough
        default:
            description += " an integer."
        }
        println(description)
        
        var numberA = 50
        var numberB = 100
        swapTwoInts(&numberA, b: &numberB)
        println("numberA=\(numberA) numberB=\(numberB)")

    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func calculateStatistics(#scores: [Int]) -> (min: Int, max: Int, sum: Int) {
        var min = scores[0]
        var max = scores[0]
        var sum = 0
        
        for score in scores {
            if score > max {
                max = score
            } else if score < min {
                min = score
            }
            sum += score
        }
        
        return (min, max, sum)
    }

    func swapTwoInts(inout a: Int, inout b: Int) {
        let temporaryA = a
        a = b
        b = temporaryA
    }


}

