//
//  LoginView.h
//  NewCatalog
//
//  Created by Sidney Silva on 30/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Security/Security.h>
@interface LoginViewController : UIViewController <UITextFieldDelegate,UIWebViewDelegate>

typedef enum formsToShow{
    showLoginForm,
    showSingUpForm
}   showForm;


- (instancetype)initShowingLogin:(showForm) show;
@end
