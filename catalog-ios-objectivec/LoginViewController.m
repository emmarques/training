//
//  LoginView.m
//  NewCatalog
//
//  Created by Sidney Silva on 30/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "LoginViewController.h"
#import "constants.h"
#import "ColorFromHex.h"
#import "SyncItems.h"
#import "AppDelegate.h"

@interface LoginViewController()
@property UIView *loginView;
@property UIWebView *singUpWebView;
@property showForm showForm;
@property UIActivityIndicatorView *loadingIndicatorView;
@property UITextField *userNameField;
@property UITextField *passwordField;
@end

@implementation LoginViewController



- (instancetype)initShowingLogin:(showForm) show
{
    self = [super init];
    if (self) {
        
        self.showForm = show;
        
        self.loadingIndicatorView = [[UIActivityIndicatorView alloc]initWithFrame:self.view.frame];
        self.loadingIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self.loadingIndicatorView hidesWhenStopped];
        [self.singUpWebView addSubview:self.loadingIndicatorView];
        self.loadingIndicatorView.layer.zPosition = 99;
        
        _singUpWebView.delegate = self;
        
        if (_showForm == showLoginForm) {
            _singUpWebView.frame = CGRectMake(self.view.frame.size.width*2,_singUpWebView.frame.origin.y,_singUpWebView.frame.size.width, _singUpWebView.frame.size.height);

            _loginView.frame = CGRectMake(0,_loginView.frame.origin.y,_loginView.frame.size.width, _loginView.frame.size.height);
        }else{
            [self.loadingIndicatorView startAnimating];
            
            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.lojab2c.com.br/homologacao/cadastre-se/"] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
            [_singUpWebView loadRequest:request];
            _singUpWebView.frame = CGRectMake(0,_singUpWebView.frame.origin.y,_singUpWebView.frame.size.width, _singUpWebView.frame.size.height);
            _loginView.frame = CGRectMake(-self.view.frame.size.width,_loginView.frame.origin.y,_loginView.frame.size.width, _loginView.frame.size.height);
            _loginView.alpha = 0;
        }
        
    }
    return self;
}

-(void)viewDidLoad{
    self.view.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
    
    UIButton *loginButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4-55, 40, 110, 50)];
    [loginButton setTitle:@"Login" forState:UIControlStateNormal];
    [loginButton setBackgroundColor:[ColorFromHex colorFromHexString:BASEBUTTONCOLOR]];
    [loginButton setTitleColor:[ColorFromHex colorFromHexString:BASEHIGHLIGHT] forState:UIControlStateHighlighted];
    loginButton.tag = showLoginForm;
    loginButton.layer.cornerRadius = 5;
    [loginButton addTarget:self action:@selector(changeForm:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginButton];
    
    UIButton *singUpButton = [[UIButton alloc]initWithFrame:CGRectMake(((self.view.frame.size.width/4) *3)-55, 40, 110, 50)];
    [singUpButton setTitle:@"Cadastre-se" forState:UIControlStateNormal];
    [singUpButton setBackgroundColor:[ColorFromHex colorFromHexString:BASEBUTTONCOLOR]];
    [singUpButton setTitleColor:[ColorFromHex colorFromHexString:BASEHIGHLIGHT] forState:UIControlStateHighlighted];
    singUpButton.tag = showSingUpForm;
    [singUpButton addTarget:self action:@selector(changeForm:) forControlEvents:UIControlEventTouchUpInside];
    singUpButton.layer.cornerRadius = 5;
    [self.view addSubview:singUpButton];
    
    CGRect frame = CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-100);
    _loginView = [[UIView alloc]initWithFrame:frame];
    _singUpWebView = [[UIWebView alloc]initWithFrame:frame];
    
    [self.view addSubview:_singUpWebView];
    [self.view addSubview:_loginView];
    
    [_loginView setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
    
    
    [self setUpViews];
}

-(void)setUpViews{
    _userNameField = [UITextField new];
    _userNameField.translatesAutoresizingMaskIntoConstraints = NO;
    [_userNameField setBackgroundColor:[ColorFromHex whiteColor]];
    [_userNameField setBorderStyle:UITextBorderStyleRoundedRect];
    _userNameField.keyboardType = UIKeyboardTypeEmailAddress;
    [_userNameField setDelegate:self];
    [_loginView addSubview:_userNameField];
    
   _passwordField = [UITextField new];
    _passwordField.translatesAutoresizingMaskIntoConstraints = NO;
    [_passwordField setBackgroundColor:[ColorFromHex whiteColor]];
    [_passwordField setBorderStyle:UITextBorderStyleRoundedRect];
    _passwordField.secureTextEntry = YES;
    [_loginView addSubview:_passwordField];
    
    UILabel *userNameLabel = [UILabel new];
    userNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    userNameLabel.text= @"Email:";
    [_loginView addSubview:userNameLabel];
    
    UILabel *passwordLabel = [UILabel new];
    passwordLabel.text = @"Senha:";
    passwordLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [_loginView addSubview:passwordLabel];
    
    UIButton *loginUserButton = [UIButton new];
    [loginUserButton setTitle:@"entrar" forState:UIControlStateNormal];
    [loginUserButton addTarget:self action:@selector(LoginUser) forControlEvents:UIControlEventTouchUpInside];
    [loginUserButton setBackgroundColor:[ColorFromHex colorFromHexString:BASEBUTTONCOLOR]];
    [loginUserButton setTitleColor:[ColorFromHex colorFromHexString:BASEHIGHLIGHT] forState:UIControlStateHighlighted];
    loginUserButton.translatesAutoresizingMaskIntoConstraints = NO;
    loginUserButton.layer.cornerRadius = 5;
    [self.loginView addSubview:loginUserButton];
    
    NSDictionary *views = @{@"nameField":_userNameField,
                            @"passwordField":_passwordField,
                            @"nameLabel":userNameLabel,
                            @"passwordLabel":passwordLabel,
                            @"loginUserButton":loginUserButton};
    
    
    
    [_loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[nameLabel(70)]-0-[nameField]-5-|" options:0 metrics:nil views:views]];
    [_loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[passwordLabel(70)]-0-[passwordField]-5-|" options:0 metrics:nil views:views]];
    
    [_loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[passwordLabel]-50-[loginUserButton(100)]" options:0 metrics:nil views:views]];
    
    [_loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[nameLabel(50)]-5-[passwordLabel(50)]" options:0 metrics:nil views:views]];
    [_loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[nameField(50)]-5-[passwordField(50)]" options:0 metrics:nil views:views]];

    [_loginView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[passwordField]-15-[loginUserButton(50)]" options:0 metrics:nil views:views]];
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (![self NSStringIsValidEmail:textField.text] && _loginView.frame.origin.x ==0 && ![textField.text  isEqual:@""]) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Atenção" message:@"O formato do email não é válido" delegate:self cancelButtonTitle:@"ops.." otherButtonTitles:nil];
        [alertView show];
        return false;
    }
    else
        return true;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void) changeForm:(UIButton *)button{
    if (button.tag !=_showForm) {
        if (button.tag == showLoginForm) {
            _showForm = showLoginForm;
            _loginView.alpha=1;
            [UIView animateWithDuration:0.8 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1 options:0 animations:^{
                                 _singUpWebView.frame = CGRectMake(self.view.frame.size.width*2,_singUpWebView.frame.origin.y,_singUpWebView.frame.size.width, _singUpWebView.frame.size.height);
                                 
                                 _loginView.frame = CGRectMake(0,_loginView.frame.origin.y,_loginView.frame.size.width, _loginView.frame.size.height);
                             }
                             completion:nil];
            
        }else{
            _showForm = showSingUpForm;
            [_loadingIndicatorView startAnimating];
            [UIApplication sharedApplication].networkActivityIndicatorVisible =YES;
             [UIView animateWithDuration:0.8 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1 options:0 animations:^{
                                 _singUpWebView.frame = CGRectMake(0,_singUpWebView.frame.origin.y,_singUpWebView.frame.size.width, _singUpWebView.frame.size.height);
                                 
                                 NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.lojab2c.com.br/homologacao/cadastre-se/"] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
                                 [_singUpWebView loadRequest:request];
                                 
                                 _loginView.frame = CGRectMake(-self.view.frame.size.width,_loginView.frame.origin.y,_loginView.frame.size.width, _loginView.frame.size.height);
                             }
                             completion:^(BOOL finished) {
                                _loginView.alpha= 0;
                             }];
             }
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [_loadingIndicatorView stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
 }

-(void)LoginUser{
    if([SyncItems conectOnAppUsingUser:_userNameField.text andPassword:_passwordField.text]){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.navController popViewControllerAnimated:YES];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



@end
