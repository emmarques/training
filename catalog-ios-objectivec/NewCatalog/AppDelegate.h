//
//  AppDelegate.h
//  NewCatalog
//
//  Created by Sidney Silva on 08/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JASidePanelController.h"
#import "UserProfile.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong) UINavigationController *navController;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) JASidePanelController *sidePaneController;
@property (strong) UserProfile *user;


-(void)updateUserProfileUsingDictionary:(NSDictionary*)profileDictionary;
-(void)logoutUser;
@end

