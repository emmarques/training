//
//  AppDelegate.m
//  NewCatalog
//
//  Created by Sidney Silva on 08/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "Constants.h"
#import "ColorFromHex.h"
#import "DetailItemViewController.h"
#import "SSKeychain.h"
#import "MenuTableView.h"
@interface AppDelegate ()

@property (strong) HomeViewController *homeViewController;
@property (strong) MenuTableView *menuTableVew;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
    [self.window makeKeyAndVisible];
    
    _sidePaneController = [JASidePanelController new];
    _homeViewController = [HomeViewController new];
    _navController = [[UINavigationController alloc] initWithRootViewController:_homeViewController];
    _navController.navigationItem.leftBarButtonItem = [_sidePaneController leftButtonForCenterPanel];
    [_sidePaneController setCenterPanel:_navController];
    self.menuTableVew = [MenuTableView new];
    [_sidePaneController setLeftPanel:self.menuTableVew];
    
    if (self.window.frame.size.width >320)
        _sidePaneController.leftGapPercentage = 0.35f;
    else
        _sidePaneController.leftGapPercentage = 0.6f;
    
    _navController.navigationBar.barTintColor = [ColorFromHex colorFromHexString:BASEBARBACKGROUND];
    [_navController.navigationBar setTranslucent:NO];
    [self.window setRootViewController:_sidePaneController];
    

    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)updateUserProfileUsingDictionary:(NSDictionary*)profileDictionary{
    if (!self.user) {
        self.user = [UserProfile new];
    }
    [self.user updateProfileWithDictionary:profileDictionary];
    [self.sidePaneController showCenterPanelAnimated:YES];
    MenuTableView *tableView = (MenuTableView*) self.sidePaneController.leftPanel;
    [tableView updateTable];
}

-(void)logoutUser{
    NSError *error;
     NSDictionary *test = [[SSKeychain accountsForService:HOSTNAME]objectAtIndex:0];
    if (![SSKeychain deletePasswordForService:HOSTNAME account:[test objectForKey:@"acct"] error:&error]) {
        NSLog(@"erro logout usuario: %@", error.description);
    }else{
        [self.navController popToRootViewControllerAnimated:YES];
        self.user = nil;
        [self.menuTableVew updateTable];
    }
    
}

@end
