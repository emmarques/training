//
//  CatalogCell.m
//  NewCatalog
//
//  Created by Sidney Silva on 08/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "CatalogCell.h"
#import "SyncItems.h"
#import "ColorFromHex.h"
#import "Constants.h"
@interface CatalogCell()
@property UIImageView *catalogPhoto;
@property UIActivityIndicatorView *activityIndicator;
@end
@implementation CatalogCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.catalogPhoto = [UIImageView new];
        self.catalogPhoto.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.activityIndicator = [UIActivityIndicatorView new];
        self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
        self.activityIndicator.color = [UIColor blackColor];
        [self.activityIndicator hidesWhenStopped];
        [self addSubview:_activityIndicator];
        [self addSubview:self.catalogPhoto];
        [self setupViews];
        
        self.layer.cornerRadius = 15;
        
        UIView *selectView = [[UIView alloc]initWithFrame:self.frame];
        [selectView setBackgroundColor:[ColorFromHex colorWithRed:0 green:0 blue:0 alpha:0.5] ];
        [self setSelectedBackgroundView:selectView];
        selectView.layer.cornerRadius = 15;
        
    }
    return self;
}

- (void) refreshCell: (NSString*) url{
    self.catalogPhoto.image = nil;
    [self.activityIndicator startAnimating];
    [self addSubview:self.activityIndicator];
    [SyncItems loadImageFromURLString:url completion:^(UIImage *image) {
        [self.catalogPhoto setImage:image];
        self.catalogPhoto.contentMode = UIViewContentModeScaleAspectFit;
        [self.activityIndicator stopAnimating];
    }];
}

-(void) setupViews{
    
    NSDictionary *viewsDictionary = @{@"catalogPhoto":_catalogPhoto,
                                      @"activityIndicator":_activityIndicator,
                                      };
    NSArray *constraint;
    
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[catalogPhoto]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[catalogPhoto]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[activityIndicator]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];

    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[activityIndicator]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
}

@end
