//
//  CatalogItemCell.h
//  NewCatalog
//
//  Created by Sidney Silva on 09/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogItemCell : UICollectionViewCell
- (void) refreshCell: (NSDictionary*)item;
@end
