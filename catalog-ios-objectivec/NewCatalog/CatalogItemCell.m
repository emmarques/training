//
//  CatalogItemCell.m
//  NewCatalog
//
//  Created by Sidney Silva on 09/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "CatalogItemCell.h"
#import "ColorFromHex.h"
#import "Constants.h"
#import "SyncItems.h"
@interface CatalogItemCell()
@property UIImageView *catalogItemImage;
@property UIView *colorsOfItemView;
@property UILabel *itemDescriptionLabel;
@property UILabel *itemOldPriceLabel;
@property UILabel *itemNewPriceLabel;
@property UILabel *priceConditionsLabel;
@property UIView *itemOldPriceView;
@property UIView *itemNewPriceView;
@property UIActivityIndicatorView *activityIndicator;
@end

@implementation CatalogItemCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
        
        _catalogItemImage = [UIImageView new];
       // _catalogItemImage.layer.borderWidth=1;
        _catalogItemImage.translatesAutoresizingMaskIntoConstraints = NO;
        [_catalogItemImage setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
        _catalogItemImage.contentMode = UIViewContentModeScaleAspectFit;
         [self addSubview:_catalogItemImage];
        
        _colorsOfItemView = [UIView new];
       // _colorsOfItemView.layer.borderWidth=1;
        _colorsOfItemView.translatesAutoresizingMaskIntoConstraints = NO;
        [_colorsOfItemView setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
        [self addSubview:_colorsOfItemView];
        
        _itemDescriptionLabel = [UILabel new];
        _itemDescriptionLabel.textAlignment = NSTextAlignmentCenter;
        //_itemDescriptionLabel.layer.borderWidth=1;
        _itemDescriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_itemDescriptionLabel setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
         [self addSubview:_itemDescriptionLabel];
        
        _itemOldPriceView = [UIView new];
        [self addSubview:_itemOldPriceView];
        _itemOldPriceView.translatesAutoresizingMaskIntoConstraints = NO;
        [_itemOldPriceView setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
        
        _itemOldPriceLabel = [UILabel new];
        [_itemOldPriceLabel sizeToFit];
        [self.itemOldPriceView addSubview:_itemOldPriceLabel];
        _itemOldPriceLabel.textAlignment = NSTextAlignmentCenter;
        //_itemOldPriceLabel.layer.borderWidth=1;
        _itemOldPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_itemOldPriceLabel setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
        
        _itemNewPriceView = [UIView new];
        [self addSubview:_itemNewPriceView];
        _itemNewPriceView.translatesAutoresizingMaskIntoConstraints = NO;
        [_itemNewPriceView setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
    
        _itemNewPriceLabel = [UILabel new];
        [_itemNewPriceView addSubview:_itemNewPriceLabel];
        [_itemNewPriceView sizeToFit];
        _itemNewPriceLabel.textAlignment = NSTextAlignmentCenter;
       // _itemNewPriceLabel.layer.borderWidth=1;
        _itemNewPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_itemNewPriceLabel setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
        
        _priceConditionsLabel = [UILabel new];
        [_priceConditionsLabel setTextColor:[ColorFromHex colorFromHexString:BASEHIGHLIGHT]];
        [_priceConditionsLabel sizeToFit];
        _priceConditionsLabel.textAlignment = NSTextAlignmentCenter;
       // _priceConditionsLabel.layer.borderWidth=1;
        _priceConditionsLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_priceConditionsLabel setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
        [self addSubview:_priceConditionsLabel];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:self.catalogItemImage.frame];
        self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
        [self.activityIndicator hidesWhenStopped];
        self.activityIndicator.color = [ColorFromHex blackColor];
        [self.catalogItemImage addSubview:self.activityIndicator];
        [self.activityIndicator startAnimating];
        
        if (self.frame.size.width<185) {
            self.priceConditionsLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            self.itemNewPriceLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            self.itemOldPriceLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            self.itemDescriptionLabel.font =[UIFont fontWithName:@"HelveticaNeue" size:12];
        }else{
            self.priceConditionsLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
            self.itemNewPriceLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
            self.itemOldPriceLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
            self.itemDescriptionLabel.font =[UIFont fontWithName:@"HelveticaNeue" size:15];
        }
        
        [self prepareCell];
        
        UIView *selectView = [[UIView alloc]initWithFrame:self.frame];
        [selectView setBackgroundColor:[ColorFromHex colorWithRed:0 green:0 blue:0 alpha:0.5] ];
        [self setSelectedBackgroundView:selectView];
        selectView.layer.cornerRadius = 15;
    }
    return self;
}

-(void)refreshCell:(NSDictionary *)item{
    //NSLog(@"%@",item);
    [self.activityIndicator startAnimating];
    _catalogItemImage.image = nil;
    _itemDescriptionLabel.text = [item objectForKey:@"nome"];
    _itemNewPriceLabel.text = [NSString stringWithFormat:@"Por R$%@",[item objectForKey:@"preco"]];
    if ([[item objectForKey:@"valorPromocao"]integerValue] > 0) {
        NSString *oldPrice = [NSString stringWithFormat:@"De R$%@",[item objectForKey:@"valorPromocao"]];
        NSAttributedString * text = [[NSAttributedString alloc] initWithString:oldPrice attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
        _itemOldPriceLabel.attributedText = text;
    }else
        _itemOldPriceLabel.text=@"";
    
    _priceConditionsLabel.text = [item objectForKey:@"condicoes"];
    [SyncItems loadImageFromURLString:[item objectForKey:@"imagemDestaque"] completion:^(UIImage *image) {
           _catalogItemImage.image = image;
            [self.activityIndicator stopAnimating];
       //  [self setNeedsLayout];
    }];
}

//position of itens on the cell
-(void)prepareCell{
    
    NSDictionary *viewsDictionary = @{@"catalogItemImage":_catalogItemImage,
                                      @"colorsOfItemView":_colorsOfItemView,
                                      @"itemDescription":_itemDescriptionLabel,
                                      @"itemOldPrice":_itemOldPriceView,
                                      @"itemNewPrice":_itemNewPriceView,
                                      @"priceConditions":_priceConditionsLabel,
                                      @"itemNewPriceLabel":_itemNewPriceLabel,
                                      @"itemOldPriceLabel":_itemOldPriceLabel,
                                      @"activityIndicator":_activityIndicator
                                      };
    NSArray *constraint;
    NSDictionary* metrics;
    if (self.frame.size.width<185) {
        metrics =@{@"height": @12};
    }else{
         metrics =@{@"height": @20};
    }
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[priceConditions(height)]" options:0 metrics:metrics views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[priceConditions]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[priceConditions]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[priceConditions]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[itemNewPrice(height)]" options:0 metrics:metrics views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[itemNewPrice]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[itemOldPrice(height)]" options:0 metrics:metrics views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[itemOldPrice]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[itemDescription(height)]" options:0 metrics:metrics views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[itemDescription]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[colorsOfItemView(20)]" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[colorsOfItemView]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[catalogItemImage]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[catalogItemImage]" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[itemNewPriceLabel]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[itemNewPriceLabel]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[itemOldPriceLabel]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[itemOldPriceLabel]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[activityIndicator]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[activityIndicator]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[catalogItemImage]-0-[colorsOfItemView]-0-[itemDescription]-0-[itemOldPrice]-0-[itemNewPrice]-0-[priceConditions]-0-|" options:0 metrics:nil views:viewsDictionary];
    [self addConstraints:constraint];
    
}

@end
