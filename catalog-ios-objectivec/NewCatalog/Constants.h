//
//  Constants.h
//  NewCatalog
//
//  Created by Sidney Silva on 08/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#ifndef NewCatalog_Constants_h
#define NewCatalog_Constants_h
#define BASECOLOR @"#d6c9c9"
#define BASEBACKGROUD @"#FFFAE6"
#define BASEBUTTONCOLOR @"#AAAAAA"
#define BASEBARBACKGROUND @"#FFCC00"
#define BASEFONTCOLOR @"#000000"
#define BASEDDESCRIPTIONCOLOR @"#000000"
#define BASEHIGHLIGHT @"#b67700"
#define BASEDISCOUNTPRICECOLOR @"000000"
#define BASETITLECOLOR @"000000"
#define BASESELECTEDCOLOR @"000000"
#define HOSTNAME @"http://private-efa435-catalog16.apiary-mock.com"
#define NOMELOJA @"Nome da loja"
#endif
