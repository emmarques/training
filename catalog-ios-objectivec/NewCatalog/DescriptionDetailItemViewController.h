//
//  DescriptionDetailItemViewController.h
//  NewCatalog
//
//  Created by Sidney Silva on 29/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionDetailItemViewController : UIViewController
- (instancetype)initWithProductCode:(long)code;
@end
