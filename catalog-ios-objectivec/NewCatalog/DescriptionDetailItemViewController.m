//
//  DescriptionDetailItemViewController.m
//  NewCatalog
//
//  Created by Sidney Silva on 29/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "DescriptionDetailItemViewController.h"
#import "Constants.h"
#import "ColorFromHex.h"
#import "SyncItems.h"

@interface DescriptionDetailItemViewController ()
@property UIWebView *webView;
@property NSString *stringHTML;
@property UIActivityIndicatorView *indicator;
@end

@implementation DescriptionDetailItemViewController

- (instancetype)initWithProductCode:(long)code
{
    self = [super init];
    if (self) {
        self.indicator = [[UIActivityIndicatorView alloc]initWithFrame:self.view.frame];
        [_indicator hidesWhenStopped];
        [SyncItems loadDataFromRequest:[NSString stringWithFormat:@"/produto/%li/descricao",code] completion:^(NSData *data) {
            _stringHTML = [[NSString alloc]initWithData:data encoding:NSStringEncodingConversionAllowLossy];
            [_indicator stopAnimating];
             [_webView loadHTMLString:_stringHTML baseURL:nil];
        }];
       
    }
    return self;
}

- (void)viewDidLoad {
    [self.view setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
     _webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    _webView.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
    [self.view addSubview:_webView];
    [self.indicator startAnimating];
    [self.webView addSubview:_indicator];
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
