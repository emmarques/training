//
//  DetailItemTableViewController.m
//  NewCatalog
//
//  Created by Sidney Silva on 14/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "DetailItemViewController.h"
#import "PhotoCollectionViewCell.h"
#import "ColorFromHex.h"
#import "Constants.h"
#import "DescriptionCollectionViewCell.h"
#import "PriceCollectionViewCell.h"
#import "ImageSliderViewController.h"
#import "SyncItems.h"
#import "DescriptionDetailItemViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
@interface DetailItemViewController()
@property (strong) NSDictionary *product;
@property (strong) UICollectionView *collectionView;
@property (strong) NSDictionary *variations;
@property (strong) NSMutableArray *boundVariationSelected;
@property (strong) NSMutableArray *boundVariations;
@property (strong) UIButton *addToWishlistButton;
@property BOOL wishlist;
@end

@implementation DetailItemViewController

- (instancetype)initWithProductRequisition:(NSString*)productCode
{
    if (self) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString *userID;
        if (appDelegate.user) {
            userID = [NSString stringWithFormat:@"%@",appDelegate.user.userId];
        }else{
            userID = @"null";
        }
        self.collectionView.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
        [SyncItems sendRequestWithRequestType:@"POST" requestString:[NSString stringWithFormat:@"/produto/%@",productCode] dictionaryPost:@{@"clienteID":userID} completion:^(BOOL finishCorrectly, NSData *data) {
            NSError *error;
            NSDictionary *dictionary = (NSDictionary*)data;
            if (error) {
                NSLog(@"error initWthProductRequisition:%@",error.description);
            }
            _boundVariationSelected = [NSMutableArray new];
            self.product = dictionary;
            _variations = [dictionary objectForKey:@"variacoes"];
            [self.collectionView reloadData];
        }];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setUserInteractionEnabled:YES];
    
    UICollectionViewFlowLayout *flowLayout = [UICollectionViewFlowLayout new];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    flowLayout.minimumLineSpacing = 1.0f;
    self.collectionView = [[UICollectionView alloc]initWithFrame:self.view.frame collectionViewLayout:flowLayout];
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    [self.view addSubview:_collectionView];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"sizeOrColor"];
    [self.collectionView registerClass:[PhotoCollectionViewCell class] forCellWithReuseIdentifier:@"photo"];
    [self.collectionView registerClass:[DescriptionCollectionViewCell class] forCellWithReuseIdentifier:@"description"];
    [self.collectionView registerClass:[PriceCollectionViewCell class] forCellWithReuseIdentifier:@"price"];
    [self.collectionView setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - collectionView data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[_product allKeys]count]-4;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float width =self.collectionView.frame.size.width;
    if (indexPath.item == 0) {
        return CGSizeMake(width, self.collectionView.frame.size.height/2);

    }else
        return CGSizeMake(width, 70);
    
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.item==0) {
            PhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photo" forIndexPath:indexPath];
            [cell addImagesFromURLArray:[_product objectForKey:@"fotos"]];
            cell.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
            return cell;
        
    }else if (indexPath.item==1) {
            DescriptionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"description" forIndexPath:indexPath];
            [cell editDescriptionLabelWithString:[_product objectForKey:@"nome"]andMarkOfProduct:@"marca do produto"];
            cell.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
            return cell;
        
    }else if (indexPath.item==2){
            PriceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"price" forIndexPath:indexPath];
            [cell updateCell:@{@"oldPrice":[_product objectForKey:@"preco"], @"newPrice":[_product objectForKey:@"valorPromocao"],@"conditions":@""}];
            cell.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
        
                //wishlist button
        self.addToWishlistButton = [UIButton new];
        self.addToWishlistButton.frame = CGRectMake(cell.frame.size.width-50-cell.frame.size.width/7, 0, 50, 50);
        [self.addToWishlistButton addTarget:self action:@selector(addOrRemoveOnWishlist) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_addToWishlistButton];
        self.wishlist = [[_product objectForKey:@"wishlist"] boolValue];
        if (self.wishlist) {
            [self.addToWishlistButton setBackgroundImage:[UIImage imageNamed:@"wishlistSelected.png"] forState:UIControlStateNormal];
        }else{
            [self.addToWishlistButton setBackgroundImage:[UIImage imageNamed:@"wishlist.png"] forState:UIControlStateNormal];
        }
        
            return cell;
        
    }else if (indexPath.item==[self collectionView:collectionView numberOfItemsInSection:indexPath.section]-1){
        //TODO: celula de descrição.
        DescriptionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"description" forIndexPath:indexPath];
        [cell editDescriptionLabelWithString:[_product objectForKey:@"nome"]andMarkOfProduct:@"toque para descrição do produto"];
        cell.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
        return cell;
        
    }else{// passando por aqui será qualquer celula de variação (tamanho, cor, wats...)
            UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"sizeOrColor" forIndexPath:indexPath];
//            float margin = self.view.frame.size.width/7;
//            cell.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
//            UILabel *ColorLabel = [[UILabel alloc]initWithFrame:CGRectMake(margin, 0, 100, 13)];
//            ColorLabel.text = [_variations objectForKey:@"titulo"];
//            [cell addSubview:ColorLabel];
//            BoundVariationCollectionView *boundVariationItem = [[BoundVariationCollectionView alloc]initWithFrame:CGRectMake(margin, 15, self.view.frame.size.width-2*margin, cell.frame.size.height-20)];
//            boundVariationItem.tag = 1;
//            boundVariationItem.boundVariations = [_variations objectForKey:@"variacao"];
//            [boundVariationItem setDelegate:self];
//            [boundVariationItem setDataSource:boundVariationItem];
//            [cell addSubview:boundVariationItem];
            return cell;
        }
    }


//TODO: metodo loucura, se arrumar uma maneira melhor de fazer, faça.
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.item==[self collectionView:collectionView numberOfItemsInSection:indexPath.section]-1){
        [self.navigationController pushViewController:[[DescriptionDetailItemViewController alloc]initWithProductCode:[[_product objectForKey:@"codigo"] integerValue]] animated:YES];
    }
    else if (indexPath.item==0){ //default
      [self.navigationController pushViewController:[[ImageSliderViewController alloc] initWithURLImages:[_product objectForKey:@"fotos"]] animated:YES];
    }
   
}

- (void)addOrRemoveOnWishlist {
      AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.user!=nil){
        [self makeEfectOfWishList];
    
        [SyncItems sendRequestWithRequestType:@"POST" requestString:@"/ListaDesejos/addRemove" dictionaryPost:@{@"produtoID":[_product objectForKey:@"codigo"],@"clienteID":appDelegate.user.userId} completion:^(BOOL finishCorrectly, NSData *data) {
        if (finishCorrectly) {
            [self refreshWishList];
        }else{
            [self makeEfectOfWishList];
        }
    }];
    }else{
        [appDelegate.navController pushViewController:[LoginViewController new] animated:YES];
    }
}

-(void)makeEfectOfWishList{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!self.wishlist) {
        self.wishlist = YES;
        [appDelegate.user.productsInWishlist addObject:[_product objectForKey:@"codigo"]];
        [self.addToWishlistButton setBackgroundImage:[UIImage imageNamed:@"wishlistSelected.png"] forState:UIControlStateNormal];
        [UIView animateWithDuration:0.2 animations:^{
            CGAffineTransform transform = self.addToWishlistButton.transform;
            self.addToWishlistButton.transform = CGAffineTransformScale(transform, 1.5, 1.5);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.2 animations:^{
                self.addToWishlistButton.transform = CGAffineTransformIdentity;
            }];
        }];
    }else{
        self.wishlist = NO;
        [appDelegate.user.productsInWishlist removeObject:[_product objectForKey:@"codigo"]];
        [self.addToWishlistButton setBackgroundImage:[UIImage imageNamed:@"wishlist.png"] forState:UIControlStateNormal];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
