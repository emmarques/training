//
//  HomeCollectionView.h
//  NewCatalog
//
//  Created by Sidney Silva on 08/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionView : UICollectionView <UICollectionViewDataSource,UICollectionViewDelegate>
@property NSArray *catalog;
-(instancetype)initWithFrame:(CGRect)frame andCatalog:(NSDictionary*)catalog;
-(void)didChangeDictionary:(NSDictionary*) dictionary;
@end
