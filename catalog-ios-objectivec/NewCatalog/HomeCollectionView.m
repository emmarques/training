//
//  HomeCollectionView.m
//  NewCatalog
//
//  Created by Sidney Silva on 08/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "HomeCollectionView.h"
#import "CatalogCell.h"
#import "ColorFromHex.h"
#import "Constants.h"
@interface HomeCollectionView ()
@property UICollectionViewFlowLayout *flowLayout;
@end

@implementation HomeCollectionView

- (instancetype)initWithFrame:(CGRect)frame andCatalog:(NSArray*)catalog
{
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [self.flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    self.flowLayout.minimumInteritemSpacing = 5.0f;
    self.flowLayout.minimumLineSpacing = 5.0f;
    [self registerClass:[CatalogCell class] forCellWithReuseIdentifier:@"cell"];
    
    self = [super initWithFrame:frame collectionViewLayout:self.flowLayout];
    if (self) {
        self.catalog = catalog;
        [self setBackgroundColor:[ColorFromHex whiteColor]];
    }
    [self setDataSource:self];
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.catalog count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CatalogCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell refreshCell:@"http://isuba.s8.com.br/mktsuba/home/x32-dia-de-telefonia-maio.png"];
   
    return cell;
}


-(void)didChangeDictionary:(NSArray*) array{
    if (array!=self.catalog) {
        self.catalog = array;
        [self reloadData];
    }
    [self.flowLayout setItemSize:CGSizeMake(self.frame.size.width/3-5, (self.frame.size.width/3-5)*1.5)];
}

/*
// Only override drawRect: if you perform custom drawing.self.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
