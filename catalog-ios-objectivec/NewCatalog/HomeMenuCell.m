//
//  HomeMenuCell.m
//  NewCatalog
//
//  Created by Sidney Silva on 08/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "HomeMenuCell.h"
#import "ColorFromHex.h"
#import "Constants.h"
@interface HomeMenuCell()
@property (strong) UILabel *menuHomeCellLabel;
@end
@implementation HomeMenuCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self layoutSubviews];
        //para fazer a celula celecionada.

    }
    return self;
}

-(void)refreshMenuText:(NSDictionary*) text{
     _menuHomeCellLabel.text =[text objectForKey:@"nome"];
}

-(void)layoutSubviews
{
    _menuHomeCellLabel = [UILabel new];
    [self addSubview:_menuHomeCellLabel];
    [_menuHomeCellLabel setTextAlignment:NSTextAlignmentCenter];
    _menuHomeCellLabel.numberOfLines = 0;
    _menuHomeCellLabel.translatesAutoresizingMaskIntoConstraints=NO;

    
    UIView* selectedView = [UIView new];
    UIView* selectedBGView = [UIView new];
    selectedBGView.backgroundColor = [ColorFromHex colorFromHexString:BASEHIGHLIGHT];
    selectedView.layer.cornerRadius = 5;
    [selectedView addSubview:selectedBGView];
    self.selectedBackgroundView = selectedView;
    
    selectedView.translatesAutoresizingMaskIntoConstraints=NO;
    selectedBGView.translatesAutoresizingMaskIntoConstraints=NO;
    
  [self addConstraints:  [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[label(30)]-0-|"
                                                                 options:0
                                                                 metrics:nil
                                                                    views:@{@"label":_menuHomeCellLabel}]];

   [self addConstraints:  [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[label]-0-|"
                                                                  options:0
                                                                  metrics:nil
                                                                    views:@{@"label":_menuHomeCellLabel}]];
    
    
    
    [self addConstraints:  [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[selectedView]-0-|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:@{@"selectedView":selectedView}]];
    
    [self addConstraints:  [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[selectedView]-0-|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:@{@"selectedView":selectedView}]];
    
    
    
    [selectedView addConstraints:  [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[selectedBGView]-0-|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:@{@"selectedBGView":selectedBGView}]];
    
    [selectedView addConstraints:  [NSLayoutConstraint constraintsWithVisualFormat:@"V:[selectedBGView(4)]-0-|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:@{@"selectedBGView":selectedBGView}]];
}
@end
