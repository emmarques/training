//
//  HomeViewController.h
//  NewCatalog
//
//  Created by Sidney Silva on 08/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperClassViewController.h"
@interface HomeViewController : SuperClassViewController <UICollectionViewDataSource,UICollectionViewDelegate>

@end
