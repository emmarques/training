//
//  HomeViewController.m
//  NewCatalog
//
//  Created by Sidney Silva on 08/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeMenuCell.h"
#import "ColorFromHex.h"
#import "Constants.h"
#import "HomeCollectionView.h"
#import "ItemCatalogViewController.h"
#import "SyncItems.h"
#import "SSKeychain.h"

@interface HomeViewController ()
@property (strong) UICollectionView *homeMenuCollectionView;
@property (strong) UICollectionViewFlowLayout *flowLayout;
@property (strong) NSArray *sectorsHomeMenu;
@property (strong) HomeCollectionView *homeColectionView;
@end

@implementation HomeViewController


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.sectorsHomeMenu = [[NSArray alloc]initWithObjects:[NSDictionary new], nil];

        //SyncItems conectOnAppUsingUser:<#(NSString *)#> andPassword:<#(NSString *)#>
        }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
    self.navigationController.title = NOMELOJA;
    
    [self setupViews];

    
    [SyncItems loadDataFromRequest:@"/categorias" completion:^(NSData *data) {
        
        NSError *error;
        NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        if (error) {
            NSLog(@"error categories:%@",error.description);
        }
        self.sectorsHomeMenu = array;
        [self.homeMenuCollectionView reloadData];
        [self.homeColectionView reloadData];
        NSIndexPath *indexPath =[NSIndexPath indexPathForItem:0 inSection:0];
        [self.homeMenuCollectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionTop];
        [self collectionView:_homeMenuCollectionView didSelectItemAtIndexPath:indexPath];

    }];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.sectorsHomeMenu count]; //contando a logo
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HomeMenuCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    [cell refreshMenuText:[self.sectorsHomeMenu objectAtIndex:indexPath.item]];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([collectionView isMemberOfClass:[HomeCollectionView class]]) {
        NSString *sectionName = [[_homeColectionView.catalog objectAtIndex:indexPath.item] objectForKey:@"nome"];
        NSString *catalog = [[_homeColectionView.catalog objectAtIndex:indexPath.item] objectForKey:@"codigo"];
        ItemCatalogViewController *viewController = [[ItemCatalogViewController alloc]initWithCatalog:catalog andSectionName:sectionName withSearchString:nil];
        [self.navigationController pushViewController:viewController animated:YES];
        [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    }else{
    NSDictionary* dic =[self.sectorsHomeMenu objectAtIndex:indexPath.item];
    [_homeColectionView didChangeDictionary:[dic objectForKey:@"subCategoria"]];
    }
}



- (void)viewDidLayoutSubviews{
    _homeMenuCollectionView.frame = CGRectMake(0, 0, _homeMenuCollectionView.superview.frame.size.width, 50);
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   }

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - position of views

-(void)setupViews{
    UIImageView *logoView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"logo.jpeg"]];
    logoView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:logoView];
    
    UIView *menuView = [UIView new];
    menuView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:menuView];
    
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [self.flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    self.flowLayout.minimumInteritemSpacing = 5.0f;
    self.flowLayout.minimumLineSpacing = 5.0f;
    self.flowLayout.estimatedItemSize = CGSizeMake(250, 50);
    self.homeMenuCollectionView = [[UICollectionView alloc]initWithFrame:menuView.frame collectionViewLayout:self.flowLayout];
    
    self.homeMenuCollectionView.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
    [self.homeMenuCollectionView registerClass:[HomeMenuCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.homeMenuCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"LogoCell"];
    [self.view addSubview:self.homeMenuCollectionView];
    [self.homeMenuCollectionView setDataSource:self];
    [self.homeMenuCollectionView setDelegate:self];
    
    [menuView addSubview:_homeMenuCollectionView];
    
    _homeColectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    //view para exibir os setores do catalogo
    UIView *sectionCollectionViewView = [UIView new];
    sectionCollectionViewView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:sectionCollectionViewView];
    
    //collectionVIew que mostra os setores (calçados, blusa, saia...)
    self.homeColectionView = [[HomeCollectionView alloc]initWithFrame:sectionCollectionViewView.frame andCatalog:[self.sectorsHomeMenu objectAtIndex:0]];
    [sectionCollectionViewView addSubview:_homeColectionView];
    [_homeColectionView setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
    [_homeColectionView setDelegate:self];
    _homeColectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSDictionary *viewsDictionary = @{@"homeMenuCollectionView":menuView,
                                      @"logoView":logoView,
                                      @"sectionCollectionView":sectionCollectionViewView,
                                      @"HomeCollectionView":_homeColectionView,
                                      };
    NSArray *constraint;
    
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[logoView(50)]"
                                                         options:0
                                                         metrics:nil
                                                           views:viewsDictionary];
    [self.view addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[logoView(50)]"
                                                         options:0
                                                         metrics:nil
                                                           views:viewsDictionary];
    [self.view addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[logoView]-5-[sectionCollectionView]-5-|"
                                                         options:0
                                                         metrics:nil
                                                           views:viewsDictionary];
    [self.view addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[homeMenuCollectionView(50)]"
                                                         options:0
                                                         metrics:nil
                                                           views:viewsDictionary];
    [self.view addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[homeMenuCollectionView]"
                                                         options:0
                                                         metrics:nil
                                                           views:viewsDictionary];
    [self.view addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[logoView]-5-[homeMenuCollectionView]-5-|"
                                                         options:0
                                                         metrics:nil
                                                           views:viewsDictionary];
    
    [self.view addConstraints:constraint];
    
    constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[sectionCollectionView]-5-|"
                                                         options:0
                                                         metrics:nil
                                                           views:viewsDictionary];
    
    [self.view addConstraints:constraint];
    
    //constraint da sectionCollectionView
    
        constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[HomeCollectionView]-0-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:viewsDictionary];
        [sectionCollectionViewView addConstraints:constraint];
    
        constraint= [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[HomeCollectionView]-0-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:viewsDictionary];
        [sectionCollectionViewView addConstraints:constraint];
    
}

#pragma mark - conection

-(void)viewDidAppear:(BOOL)animated{
    NSDictionary* profileAccount = [[SSKeychain accountsForService:HOSTNAME] objectAtIndex:0];
    if (profileAccount) {
        NSString *username = [profileAccount objectForKey:@"acct"];
        if([SyncItems conectOnAppUsingUser:username andPassword:[SSKeychain passwordForService:HOSTNAME account:username]]){
            [self refreshWishList];
        }
    }
    [self refreshWishList];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
