//
//  ImageSliderViewController.h
//  NewCatalog
//
//  Created by Sidney Silva on 15/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KASlideShow.h"
@interface ImageSliderViewController : UIViewController
- (instancetype)initWithURLImages:(NSArray *)images;
@end
