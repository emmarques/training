//
//  ImageSliderViewController.m
//  NewCatalog
//
//  Created by Sidney Silva on 15/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "ImageSliderViewController.h"
#import "SyncItems.h"
#import "ColorFromHex.h"
#import "Constants.h"
@interface ImageSliderViewController ()
@property (strong) KASlideShow *slideshow;
@property (strong) NSArray *imagesURL;
@end

@implementation ImageSliderViewController

- (instancetype)initWithURLImages:(NSArray *)images
{
    self = [super init];
    if (self) {
        _imagesURL = images;
       
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _slideshow = [[KASlideShow alloc]initWithFrame:self.view.frame];
    [self.view setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
    for (NSString *url in _imagesURL) {
        [SyncItems loadImageFromURLString:url completion:^(UIImage *image) {
            [_slideshow addImage:image];
        }];
    }
    
    [self.view setUserInteractionEnabled:YES];
    [_slideshow setTransitionDuration:.5]; // Transition duration
    [_slideshow setTransitionType:KASlideShowTransitionSlide]; // Choose a transition type (fade or slide)
    [_slideshow setImagesContentMode:UIViewContentModeScaleAspectFit]; // Choose a content mode for images to display
    [_slideshow addGesture:KASlideShowGestureSwipe]; // Gesture to go previous/next directly on the image
    [self.view addSubview:_slideshow];
    
    UIButton *previousButton = [[UIButton alloc]initWithFrame:CGRectMake(5, self.view.frame.size.height/2-60, 30, 60)];
    [previousButton setTitle:@"<" forState:UIControlStateNormal];
    [previousButton setTitleColor:[ColorFromHex colorFromHexString:BASEBUTTONCOLOR] forState:UIControlStateNormal];
    [previousButton addTarget:_slideshow action:@selector(previous) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:previousButton];
    
     UIButton *nextButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-35, self.view.frame.size.height/2-60, 30, 60)];
    [nextButton setTitle:@">" forState:UIControlStateNormal];
    [nextButton setTitleColor:[ColorFromHex colorFromHexString:BASEBUTTONCOLOR] forState:UIControlStateNormal];
    [nextButton addTarget:_slideshow action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextButton];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
