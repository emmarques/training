//
//  ItemCatalogCollectionViewController.h
//  NewCatalog
//
//  Created by Sidney Silva on 10/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SidePanelsViewController.h"
#import "SuperClassViewController.h"
@interface ItemCatalogViewController : SuperClassViewController <UICollectionViewDataSource,UICollectionViewDelegate>
-(instancetype)initWithCatalog:(NSString*)catalog andSectionName:(NSString*)sectionName withSearchString:(NSString *) searchString;
@end
