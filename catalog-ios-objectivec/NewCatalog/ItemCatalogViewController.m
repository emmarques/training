//
//  ItemCatalogCollectionViewController.m
//  NewCatalog
//
//  Created by Sidney Silva on 10/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "ItemCatalogViewController.h"
#import "ColorFromHex.h"
#import "Constants.h"
#import "CatalogItemCell.h"
#import "LoadingCollectionViewCell.h"
#import "DetailItemViewController.h"
#import "SyncItems.h"
#import "AppDelegate.h"
@interface ItemCatalogViewController ()
@property (strong) UICollectionViewFlowLayout *flowLayout;
@property (strong) NSArray *originalCatalog;
@property (strong) NSMutableArray *showCatalog; //catalogo mostrado
@property (strong) LoadingCollectionViewCell *loadingCell;
@property (strong) UICollectionView *collectionView;
@property (strong) NSString *sectionID;
@property int pageCounter;
@property BOOL loadingIntems;
@property NSString *searchString;
@end

@implementation ItemCatalogViewController

static NSString * const reuseIdentifier = @"Cell";

-(instancetype)initWithCatalog:(NSString*)catalog andSectionName:(NSString*)sectionName withSearchString:(NSString *) searchString{
    self = [super init];
    if (self) {
        self.view.backgroundColor = [ColorFromHex colorFromHexString:BASEBACKGROUD];
        ///produtos/categoria/231/pagina/1
        self.pageCounter =1;
        self.loadingIntems = YES;
        if(searchString){
            self.searchString = searchString;
        }else{
            self.searchString = @"NULL";
        }
        
        if(catalog){
           self.sectionID = catalog;
        }else{
            self.sectionID = @"NULL";
        }
    
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSString* userId = [appDelegate.user.userId stringValue];
        if (!userId) {
            userId = @"NULL";
        }
        
            [SyncItems sendRequestWithRequestType:@"POST" requestString:[NSString stringWithFormat:@"/produtos/categoria/%@/searchString/%@/pagina/%d",self.sectionID,self.searchString,self.pageCounter] dictionaryPost:@{@"clienteID":userId} completion:^(BOOL finishCorrectly, NSData *data) {
                if (finishCorrectly) {
                    
                    self.showCatalog = [NSMutableArray arrayWithArray:(NSArray*)data];
                    [self.collectionView reloadData];

                }else{
                    [_loadingCell loadEndOfCatalog];
                }
                self.loadingIntems = NO;
        }];
       
       self.navigationItem.title = sectionName;
    }
    
    self.collectionView.allowsSelection = YES;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.flowLayout.minimumInteritemSpacing = 5.0f;
    self.flowLayout.minimumLineSpacing = 5.0f;
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-70) collectionViewLayout:self.flowLayout];
    [self.collectionView setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    [self.view addSubview:self.collectionView];
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[CatalogItemCell class] forCellWithReuseIdentifier:reuseIdentifier];
    [self.collectionView registerClass:[LoadingCollectionViewCell class] forCellWithReuseIdentifier:@"loading"];
    UIBarButtonItem *newBackButton =[[UIBarButtonItem alloc] initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = newBackButton;
    
    // Do any additional setup after loading the view.
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//TODO: mudar a requisição do item para o produto
    NSDictionary *product =[self.showCatalog objectAtIndex:indexPath.item];
    DetailItemViewController *detailItem =[[DetailItemViewController alloc] initWithProductRequisition:[product objectForKey:@"codigo"]];
    [self.navigationController pushViewController:detailItem animated:YES];
}

#pragma mark - size of collection cell

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.item == [self.showCatalog count]) {
        return CGSizeMake(self.collectionView.frame.size.width, 30);
    }
    
    float width;
    float height;
    
    if (self.view.frame.size.width < 375) { //para iphones menores terem apenas duas colunas
        width = collectionView.frame.size.width/2-5;
    }else{
       width =self.collectionView.frame.size.width/3-6;
    }
    
    height = width*1.5;
    
    return CGSizeMake(width, height);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_showCatalog count]+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.item == [_showCatalog count]){
        [self loadMoreItems];
        _loadingCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"loading" forIndexPath:indexPath];
        [_loadingCell initLoading];
        return _loadingCell;
    }else{
        CatalogItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
        [cell refreshCell:[self.showCatalog objectAtIndex:indexPath.item]];
        return cell;
    }
}

#pragma mark - server comunication

-(void)loadMoreItems{
    if (!self.loadingIntems) {
        self.loadingIntems = YES;
    [_loadingCell loadEndOfCatalog];
    self.pageCounter ++;
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString* userId = [appDelegate.user.userId stringValue];
        if (!userId) {
            userId = @"NULL";
        }
        
        
        [SyncItems sendRequestWithRequestType:@"POST" requestString:[NSString stringWithFormat:@"/produtos/categoria/%@/searchString/%@/pagina/%d",self.sectionID.description,self.searchString,self.pageCounter] dictionaryPost:@{@"clienteID":userId} completion:^(BOOL finishCorrectly, NSData *data) {
            NSArray *array;
            if (finishCorrectly && data) {
                array = [NSMutableArray arrayWithArray:(NSArray*)data];
                NSNumber *count = [NSNumber numberWithFloat:[self.showCatalog count]]; //valor total de itens até agora
                NSNumber *newCount = [NSNumber numberWithFloat:[array count]]; //itens novos recebidos
                [_showCatalog addObjectsFromArray:array];
                NSMutableArray *insertIndexes = [NSMutableArray new];
                for (int item = [count intValue]; item < [count intValue] + [newCount intValue]; item++) {
            
                    [insertIndexes addObject:[NSIndexPath indexPathForRow:item
                                                        inSection:0]];

                }
             [self.collectionView insertItemsAtIndexPaths:insertIndexes];
                 [[self collectionView] setNeedsLayout];
            }
         else{
             [_loadingCell loadEndOfCatalog];
         }
            self.loadingIntems = NO;
        }];
    }
}

#pragma mark <UICollectionViewDelegate>


// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}



// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.    
}
/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
