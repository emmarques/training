//
//  LoadingCollectionViewCell.h
//  NewCatalog
//
//  Created by Sidney Silva on 13/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingCollectionViewCell : UICollectionViewCell

-(void)initLoading;

-(void)loadEndOfCatalog;
@end
