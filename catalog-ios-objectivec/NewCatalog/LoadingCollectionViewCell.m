//
//  LoadingCollectionViewCell.m
//  NewCatalog
//
//  Created by Sidney Silva on 13/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "LoadingCollectionViewCell.h"
@interface LoadingCollectionViewCell()
@property UIActivityIndicatorView *indicator;
@property UILabel *endOfCollection;
@end

@implementation LoadingCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       
        _indicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.frame.size.width/2, self.frame.size.height/2-10, 15, 15)];
        [_indicator hidesWhenStopped];
        [_indicator startAnimating];
        _indicator.hidden = NO;
        _indicator.color = [UIColor blackColor];
        [self bringSubviewToFront:_indicator];
        [self addSubview:_indicator];
        
        _endOfCollection = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _endOfCollection.text = @"sem mais resultados";
        _endOfCollection.textAlignment = NSTextAlignmentCenter;
        _endOfCollection.alpha = 1;
        [self addSubview:_endOfCollection];
    }
    return self;
}

-(void)loadEndOfCatalog{
    [_indicator stopAnimating];
    _endOfCollection.alpha = 1;
    [self setNeedsLayout];
}

-(void)initLoading{
    [self.indicator startAnimating];
    _endOfCollection.alpha = 0;
    [self setNeedsLayout];
}
@end
