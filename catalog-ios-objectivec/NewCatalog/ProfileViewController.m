//
//  ProfileViewController.m
//  NewCatalog
//
//  Created by Sidney Silva on 05/05/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "ProfileViewController.h"
#import "Constants.h"
#import "ColorFromHex.h"
#import "SyncItems.h"
#import "AppDelegate.h"
@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    self.view.backgroundColor =[ColorFromHex colorFromHexString:BASEBACKGROUD];
    
    [super viewDidLoad];
    UIButton *logoutButton = [[UIButton alloc]initWithFrame:CGRectMake(100, 100, 100, 50)];
    [logoutButton setTitle:@"sair" forState:UIControlStateNormal];
    [logoutButton setBackgroundColor:[ColorFromHex colorFromHexString:BASEHIGHLIGHT]];
    [logoutButton addTarget:self action:@selector(logoutUser) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutButton];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)logoutUser{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate logoutUser];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
