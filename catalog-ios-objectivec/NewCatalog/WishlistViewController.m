//
//  WishlistViewController.m
//  NewCatalog
//
//  Created by Sidney Silva on 07/05/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "WishlistViewController.h"
#import "Constants.h"
#import "ColorFromHex.h"
#import "AppDelegate.h"
#import "SyncItems.h"
#import "DetailItemViewController.h"
@interface WishlistViewController ()
@property NSArray *wishlistArray;
@end

@implementation WishlistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView *tableView =[[UITableView alloc]initWithFrame:self.view.frame];
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [tableView setBackgroundColor:[ColorFromHex colorFromHexString:BASEBACKGROUD]];
    tableView.estimatedRowHeight = 120;
    tableView.rowHeight = 120;
    [tableView setDelegate:self];
    [tableView setDataSource:self];
    [self.view addSubview:tableView];
    
    AppDelegate *appDelegate =[[UIApplication sharedApplication] delegate];
    
    [SyncItems sendRequestWithRequestType:@"POST" requestString:@"/listaDesejos/produtos" dictionaryPost:@{@"clienteID":appDelegate.user.userId} completion:^(BOOL finishCorrectly, NSData *data) {
        if (finishCorrectly) {
            _wishlistArray = (NSArray*)data;
            [tableView reloadData];

        }
         }];
   
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *product = [_wishlistArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    
    cell.imageView.image = [UIImage imageNamed:@"wishlistSelected.png"];
       // UIImageView *photoImage = [UIImageView alloc]initWithFrame:CGRectMake(10, 0, cell.frame.size.height*0.6, cell.frame.size.width)
    
    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc]initWithFrame:cell.imageView.frame];
    indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [indicatorView hidesWhenStopped];
    [indicatorView startAnimating];
    [cell.imageView addSubview:indicatorView];
    [cell setBackgroundColor:[UIColor clearColor]];
    [SyncItems loadImageFromURLString:[product objectForKey:@"imagemDestaque"] completion:^(UIImage *image) {
        cell.imageView.image = image;
        [indicatorView stopAnimating];
    }];
    
    UILabel *descriptionLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.imageView.image.size.width +20, 5, cell.frame.size.width, 80)];
    [descriptionLabel setNumberOfLines:2];
    descriptionLabel.text = [product objectForKey:@"nome"];
    [cell addSubview:descriptionLabel];
    
    UILabel *defaultPrice = [[UILabel alloc]initWithFrame:CGRectMake(cell.imageView.image.size.width +20, 70, 100, 20)];
    [defaultPrice setNumberOfLines:1];
    [cell addSubview:defaultPrice];
    
    if (![[product objectForKey:@"valorPromocao"]  isEqual: @""]) {
        UILabel *newPrice = [[UILabel alloc]initWithFrame:CGRectMake(cell.imageView.image.size.width +20, 90, cell.frame.size.width, 20)];
        [newPrice setNumberOfLines:1];
        newPrice.text = [NSString stringWithFormat:@"Por: R$%.2f",[[product objectForKey:@"valorPromocao"] doubleValue]];
        [newPrice sizeToFit];
        [cell addSubview:newPrice];
        
        defaultPrice.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"De: %.2f",[[product objectForKey:@"preco"] doubleValue]] attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
         [defaultPrice sizeToFit];
    }else{
        defaultPrice.text = [NSString stringWithFormat:@"Por: R$%.2f",[[product objectForKey:@"preco"] doubleValue]];
         [defaultPrice sizeToFit];
    }
   
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_wishlistArray count];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *product = [_wishlistArray objectAtIndex:indexPath.row];
    AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [appdelegate.navController pushViewController:[[DetailItemViewController alloc] initWithProductRequisition:[product objectForKey:@"codigo"]] animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
