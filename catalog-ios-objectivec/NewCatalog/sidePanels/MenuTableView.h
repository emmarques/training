//
//  MenuTableView.h
//  NewCatalog
//
//  Created by Sidney Silva on 16/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserProfile.h"
@interface MenuTableView : UIViewController <UITableViewDelegate,UITableViewDataSource>
-(void)updateTable;
@end
