//
//  MenuTableView.m
//  NewCatalog
//
//  Created by Sidney Silva on 16/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "MenuTableView.h"
#import "ColorFromHex.h"
#import "Constants.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "ItemCatalogViewController.h"
#import "ProfileViewController.h"

@interface MenuTableView()
@property UITableView *tableView;
@end

@implementation MenuTableView


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)viewDidLoad{
    _tableView = [[UITableView alloc]initWithFrame:self.view.frame];
    [self.view addSubview:_tableView];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.estimatedRowHeight = 100;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 4;
            break;
            
        default:
            return 7;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0 && indexPath.row ==0) {
       
        UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if(appDelegate.user){
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
            cell.textLabel.text = appDelegate.user.name;
            cell.detailTextLabel.text = appDelegate.user.email;
            return cell;
        }else{
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.text = @"login ou cadastre-se:";
            UIButton *loginButton = [[UIButton alloc]initWithFrame:CGRectMake(5, 70, 80, 30)];
            [loginButton setTitleColor:[ColorFromHex colorFromHexString:BASEHIGHLIGHT] forState:UIControlStateNormal];
            [loginButton setTitle:@"login" forState:UIControlStateNormal];
            loginButton.tag = 0;
            [loginButton addTarget:self action:@selector(openLoginViewController:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:loginButton];
            
            UIButton *singUpButton = [[UIButton alloc]initWithFrame:CGRectMake(80, 70, 100, 30)];
            [singUpButton setTitleColor:[ColorFromHex colorFromHexString:BASEHIGHLIGHT] forState:UIControlStateNormal];
            [singUpButton setTitle:@"cadastre-se" forState:UIControlStateNormal];
            singUpButton.tag = 1;
            [singUpButton addTarget:self action:@selector(openLoginViewController:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:singUpButton];
            
            return cell;
        }
    }
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    cell.textLabel.text = [NSString stringWithFormat:@"linha %li",(long)indexPath.row];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"section %li", (long)indexPath.section];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (indexPath.row==0 && indexPath.section==0 && appDelegate.user) {
        [appDelegate.navController pushViewController:[ProfileViewController new] animated:YES];
        [appDelegate.sidePaneController showCenterPanelAnimated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row ==0) {
        return 120;
    }
    return UITableViewAutomaticDimension;
}

-(void)openLoginViewController:(UIButton *)button{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(button.tag==0){
        LoginViewController *login = [[LoginViewController alloc]initShowingLogin:showLoginForm];
        [appDelegate.navController pushViewController:login animated:YES];
    }else{
        LoginViewController *login = [[LoginViewController alloc]initShowingLogin:showSingUpForm];
        [appDelegate.navController pushViewController:login animated:YES];
    }
    [appDelegate.sidePaneController showCenterPanelAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //[self.tableView reloadData];
}

-(void)updateTable{
    [self.tableView reloadData];
}

@end
