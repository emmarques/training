//
//  SidePanelsViewController.m
//  NewCatalog
//
//  Created by Sidney Silva on 16/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "SidePanelsViewController.h"
#import "ItemCatalogViewController.h"
@interface SidePanelsViewController ()
@property bool rightPanelIsOpen;
@property bool leftPanelIsOpen;
@end

@implementation SidePanelsViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(openLeftPanel)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(openRightPanel)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)openRightPanel{
    if (!_rightPanelIsOpen && [self isMemberOfClass:[ItemCatalogViewController class]]) {
        NSLog(@"open right Panel");
    }
}

-(void)openLeftPanel{
    if (!_leftPanelIsOpen) {
        NSLog(@"open left Panel");
    }
}

-(void)MakeAnimationLeft{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
