//
//  DescriptionTableViewCell.m
//  NewCatalog
//
//  Created by Sidney Silva on 14/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "DescriptionCollectionViewCell.h"
#import "Constants.h"
#import "ColorFromHex.h"

@interface DescriptionCollectionViewCell()
@property UILabel *descriptionLabel;
@property UILabel *mark;
@end
@implementation DescriptionCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.descriptionLabel = [UILabel new];
        [self addSubview:_descriptionLabel];
        self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.mark = [UILabel new];
        self.mark.translatesAutoresizingMaskIntoConstraints = NO;
        self.mark.textColor = [ColorFromHex colorFromHexString:BASEHIGHLIGHT];
        [self addSubview:self.mark];
        
        [self setUpViews];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

-(void)editDescriptionLabelWithString:(NSString *)description andMarkOfProduct:(NSString*)mark{
    _mark.text = mark;
    _descriptionLabel.text = description;
}

-(void)setUpViews{
    NSDictionary *views = @{@"descriptionLabel":_descriptionLabel,@"markLabel":_mark};

    NSDictionary *metrics = @{@"margin":[NSNumber numberWithFloat:[UIScreen mainScreen].bounds.size.width/7]};
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[descriptionLabel]-margin-|"  options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[markLabel]-margin-|"  options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[markLabel]-0-[descriptionLabel(15)]-5-|"  options:0 metrics:metrics views:views]];
    
}


@end
