//
//  PhotoTableViewCell.m
//  NewCatalog
//
//  Created by Sidney Silva on 14/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "PhotoCollectionViewCell.h"
#import "SyncItems.h"
#import "ColorFromHex.h"
#import "Constants.h"
@interface PhotoCollectionViewCell()
@property (strong,nonatomic) KASlideShow * slideshow;
@property (strong, nonatomic) UIButton *previousButton;
@property (strong, nonatomic) UIButton *nextButton;
@property (strong) NSMutableArray *images;
@end

@implementation PhotoCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _slideshow = [KASlideShow new];
        [_slideshow setDelegate:self];
        [_slideshow setTransitionDuration:.5]; // Transition duration
        [_slideshow setTransitionType:KASlideShowTransitionSlide]; // Choose a transition type (fade or slide)
        [_slideshow setImagesContentMode:UIViewContentModeScaleAspectFit]; // Choose a content mode for images to display
        [_slideshow addGesture:KASlideShowGestureSwipe]; // Gesture to go previous/next directly on the image
        _slideshow.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_slideshow];
        
        self.images = [NSMutableArray new];
        _previousButton = [UIButton new];
        _previousButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_previousButton setTitle:@"<" forState:UIControlStateNormal];
        [_previousButton setTitleColor:[ColorFromHex colorFromHexString:BASEBUTTONCOLOR] forState:UIControlStateNormal];
        [_previousButton addTarget:_slideshow action:@selector(previous) forControlEvents:UIControlEventTouchDown];
        [self addSubview:_previousButton];
        
        _nextButton = [UIButton new];
        _nextButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_nextButton setTitle:@">" forState:UIControlStateNormal];
        [_nextButton setTitleColor:[ColorFromHex colorFromHexString:BASEBUTTONCOLOR] forState:UIControlStateNormal];
        [_nextButton addTarget:_slideshow action:@selector(next) forControlEvents:UIControlEventTouchDown];
        [self addSubview:_nextButton];
        
        [self loadItems];
    }
    return self;
}

-(void)addImagesFromURLArray:(NSArray *)urls{
    for(NSString *url in urls){
        [SyncItems loadImageFromURLString:url completion:^(UIImage *image) {
            [_slideshow addImage:image];
        }];
    }
}

-(void)loadItems{
    NSDictionary *viewsDictionary = @{@"slideshow":_slideshow,
                                      @"nextButton":_nextButton,
                                      @"previousButton":_previousButton,
                                      };
    NSDictionary *metrics = @{@"height": [NSNumber numberWithFloat:self.frame.size.height/2]};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[slideshow]-5-|"
                                                         options:0
                                                         metrics:nil
                                                           views:viewsDictionary]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[slideshow]-5-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:viewsDictionary]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[nextButton]-5-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:viewsDictionary]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[nextButton(height)]"
                                                                 options:0
                                                                 metrics:metrics
                                                                   views:viewsDictionary]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[nextButton]-height-|"
                                                                 options:0
                                                                 metrics:metrics
                                                                   views:viewsDictionary]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[previousButton]"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:viewsDictionary]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[previousButton(height)]"
                                                                 options:0
                                                                 metrics:metrics
                                                                   views:viewsDictionary]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[previousButton]-height-|"
                                                                 options:0
                                                                 metrics:metrics
                                                                   views:viewsDictionary]];
}



@end
