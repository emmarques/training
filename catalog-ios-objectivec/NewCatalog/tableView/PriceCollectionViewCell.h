//
//  PriceTableViewCell.h
//  NewCatalog
//
//  Created by Sidney Silva on 14/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PriceCollectionViewCell : UICollectionViewCell
-(void)updateCell:(NSDictionary *)prices;
@end
