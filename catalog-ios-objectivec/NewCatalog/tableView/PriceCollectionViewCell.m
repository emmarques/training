//
//  PriceTableViewCell.m
//  NewCatalog
//
//  Created by Sidney Silva on 14/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "PriceCollectionViewCell.h"
#import "ColorFromHex.h"
#import "Constants.h"
#import "SyncItems.h"
#import "AppDelegate.h"
@interface PriceCollectionViewCell()
@property (strong) UILabel *actualPrice;
@property (strong) UILabel *oldPrice;
@property (strong) UILabel *conditions;

@end
@implementation PriceCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.actualPrice = [UILabel new];
        self.actualPrice.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_actualPrice];
        
        self.oldPrice = [UILabel new];
        self.oldPrice.translatesAutoresizingMaskIntoConstraints = NO;
    
        
        self.conditions = [UILabel new];
        self.conditions.translatesAutoresizingMaskIntoConstraints = NO;
        self.conditions.textColor = [ColorFromHex colorFromHexString:BASEHIGHLIGHT];
        [self addSubview:_conditions];
        
        
        
        [self setUpViews];
    }
    return self;
}

-(void)updateCell:(NSDictionary *)prices{
    NSAttributedString * title = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"R$:%@",[prices objectForKey:@"oldPrice"]] attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
    [_oldPrice setAttributedText:title];
    _actualPrice.text = [NSString stringWithFormat:@"R$:%@",[prices objectForKey:@"newPrice"]];
    _conditions.text = [prices objectForKey:@"conditions"];
    
}

-(void)setUpViews{
    
    NSDictionary *views = @{@"atualPrice":_actualPrice,
                            @"oldPrice":_oldPrice,
                            @"conditions":_conditions};
    
    NSDictionary *metrics = @{@"midleHeight":[NSNumber numberWithFloat:self.frame.size.height/2],
                              @"margin":[NSNumber numberWithFloat:[UIScreen mainScreen].bounds.size.width/7]};//margem que ocupa 1/7 da tela
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[conditions]-margin-|"  options:0 metrics:metrics views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[atualPrice]-margin-|"  options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[atualPrice(midleHeight)]-0-[conditions]-0-|"  options:0 metrics:metrics views:views]];

    
}



@end
