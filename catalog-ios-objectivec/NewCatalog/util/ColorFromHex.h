//
//  ColorFromHex.h
//  NewCatalog
//
//  Created by Sidney Silva on 08/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ColorFromHex : UIColor
+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
