//
//  SuperClassViewController.h
//  NewCatalog
//
//  Created by Sidney Silva on 07/05/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//



//superclasse criada para armazenar os itens comuns em todas as viewControllers.

#import <UIKit/UIKit.h>

@interface SuperClassViewController : UIViewController <UISearchBarDelegate>
-(void)refreshWishList; //atualiza o menu da barra superior
@end
