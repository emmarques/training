//
//  SuperClassViewController.m
//  NewCatalog
//
//  Created by Sidney Silva on 07/05/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "SuperClassViewController.h"
#import "AppDelegate.h"
#import "wishlistViewController.h"
#import "LoginViewController.h"
#import "ItemCatalogViewController.h"
@interface SuperClassViewController ()
@property UILabel *label;
@property (strong) NSMutableArray *rightButtonItems;
@property (strong)   UIBarButtonItem *searchBarItem;
@property (strong)   UIBarButtonItem *cancelSearchButton;
@end

@implementation SuperClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     _rightButtonItems = [[NSMutableArray alloc]initWithArray:self.navigationItem.rightBarButtonItems];
    
     AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"wishlistSelected.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(openWishlistViewController)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 45, 45)];
    _label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 20)];
    [_label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:13]];
    if(appDelegate.user!=nil){
    [_label setText:[NSString stringWithFormat:@"%li",(unsigned long)[appDelegate.user.productsInWishlist count]]];
    [_label setBackgroundColor:[UIColor whiteColor]];
    }
    _label.textAlignment = NSTextAlignmentCenter;
    [_label sizeToFit];
    [_label setTextColor:[UIColor blackColor]];
    
    _label.layer.masksToBounds = YES;
    _label.layer.cornerRadius = 5;
    [button addSubview:_label];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
   
    [_rightButtonItems addObject:barButton];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithArray:_rightButtonItems];
    
    self.rightButtonItems = [[NSMutableArray alloc]initWithArray:self.navigationItem.rightBarButtonItems];
    _searchBarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(showSerachBar)];
    [_rightButtonItems addObject:_searchBarItem];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithArray:_rightButtonItems];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)openWishlistViewController{
    NSLog(@"openWishlistViewController");
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(appDelegate.user!=nil){
        [appDelegate.navController pushViewController:[WishlistViewController new] animated:YES];
    }else{
         [appDelegate.navController pushViewController:[LoginViewController new] animated:YES];
    }
}

-(void)refreshWishList{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if(appDelegate.user!=nil){
        [_label setText:[NSString stringWithFormat:@"%lu",(unsigned long)[appDelegate.user.productsInWishlist count]]];
        [_label setBackgroundColor:[UIColor whiteColor]];
    }else{
        [_label setText:[NSString stringWithFormat:@""]];
    }
    [_label sizeToFit];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self refreshWishList];
}

#pragma mark - navigation bar actions

-(void)showSerachBar{
    UISearchBar *searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width/3, 50)];
    [searchBar setEnablesReturnKeyAutomatically:NO];
    //searchBar.showsCancelButton=YES;
    [searchBar becomeFirstResponder];
    [searchBar setDelegate:self];
    
    _cancelSearchButton =[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(hideSearchBar)];
    [_rightButtonItems addObject:_cancelSearchButton];
    
    [_rightButtonItems removeObject: _searchBarItem];
    _searchBarItem = [[UIBarButtonItem alloc] initWithCustomView:searchBar];
    [_rightButtonItems addObject:_searchBarItem];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithArray:_rightButtonItems];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.navController pushViewController:[[ItemCatalogViewController alloc] initWithCatalog:nil andSectionName:searchBar.text withSearchString:searchBar.text] animated:YES];
    UIView *bar =_searchBarItem.customView;
    [bar resignFirstResponder];
}

-(void)hideSearchBar{
    
    [_rightButtonItems removeObject:_cancelSearchButton];
    [_rightButtonItems removeObject:_searchBarItem];
    
    _searchBarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(showSerachBar)];
    [_rightButtonItems addObject:_searchBarItem];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithArray:_rightButtonItems];
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
