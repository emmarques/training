//
//  SyncItems.h
//  NewCatalog
//
//  Created by Sidney Silva on 09/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface SyncItems : NSObject
+(void)loadImageFromURLString:(NSString*)string completion:(void(^)(UIImage* image))completion;
+(void)loadDataFromRequest:(NSString*)string completion:(void(^)(NSData* data))completion;
+(BOOL)conectOnAppUsingUser: (NSString *)user andPassword:(NSString *)password;
+(void)sendRequestWithRequestType:(NSString*)requestType requestString:(NSString*)requestString dictionaryPost: (NSDictionary*)postDic completion:(void(^)(BOOL finishCorrectly, NSData *data))completion;

@end
