//
//  SyncItems.m
//  NewCatalog
//
//  Created by Sidney Silva on 09/04/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "SyncItems.h"
#import "Constants.h"
#import "SSKeychain.h"
#import "AppDelegate.h"
@interface SyncItems()
@property (strong) NSCache *cache;
@property (strong) NSURLRequest *request;
@property (strong, nonatomic) SyncItems *syncItem;
@end

@implementation SyncItems

+(void)loadImageFromURLString:(NSString*)string completion:(void(^)(UIImage* image))completion{
   [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                               if (error) {
                                   NSLog(@"error:%@", error.localizedDescription);
                                   //TODO: completion com imagem de arquivo não carregado
                                   [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                   completion(nil);
                               }
                               UIImage *image = [UIImage imageWithData:data];
                               [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                               completion (image);
                           }];
}

+(void)loadDataFromRequest:(NSString*)string completion:(void(^)(NSData* data))completion{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HOSTNAME,string]]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:30];
    
    [request setHTTPMethod: @"GET"];
    NSLog(@"%@%@",HOSTNAME,string);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if(connectionError){
            NSLog(@"error:%@",connectionError.localizedDescription);
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }else{
            completion(data);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
    }];
}

+(BOOL)conectOnAppUsingUser: (NSString *)username andPassword:(NSString *)password{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *loginURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/autenticacao/loginpage",HOSTNAME]]; // In ou example, it it supposed to return a JSON representation of the profile
    NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:loginURL
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:30.0f];
    [loginRequest setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"username=%@&password=%@", username, password];
    [loginRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // We send the request synchronously (there is nothing to block anyway)
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:loginRequest
                                         returningResponse:&response
                                                     error:&error];
    
    // We handle occasional errors and parse the response
    if (error != nil) {
        NSLog(@"Error: %@",error.description);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        return NO;
    } else {
        NSDictionary *profileDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        if (error != nil) {
            // We notify the delegate that something went wrong
            NSLog(@"Error: %@",error.description);
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            return NO;
        } else {
            // We notify the delegate that everything worked accoridng to the plan !
            NSLog(@"success: %@",profileDict);
            [SSKeychain setPassword:password forService:HOSTNAME account:username];
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate updateUserProfileUsingDictionary:profileDict];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            return YES;
        }
    }}

+(void)sendRequestWithRequestType:(NSString*)requestType requestString:(NSString*)requestString dictionaryPost: (NSDictionary*)postDic completion:(void(^)(BOOL finishCorrectly, NSData *data))completion{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSLog(@"%@%@",HOSTNAME,requestString);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HOSTNAME,requestString]];
    NSData *postData = [NSKeyedArchiver archivedDataWithRootObject:postDic];
    // Create the request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:requestType];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if (httpResponse.statusCode <400 && data.length ==0) {
            completion(YES,nil);//quer dizer que não espera resposta
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            return;
        }
        if (connectionError) {
            completion(NO,data);
            NSLog(@"%@",connectionError.description);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            //TODO: tratar erros
        }else{
            NSError *error;
            NSData *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            if (error) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                //TODO: tratar erros
                completion(NO,jsonData);
                NSLog(@"%@",error.description);
            }else{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                completion(YES,jsonData);
            }
        }
    }];
}



@end
