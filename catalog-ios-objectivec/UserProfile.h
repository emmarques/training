//
//  UserProfile.h
//  NewCatalog
//
//  Created by Sidney Silva on 05/05/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserProfile : NSObject
@property (strong, readonly) NSNumber *userId;
@property (strong, readonly) NSString *name;
@property (strong, readonly) NSString *email;
@property (strong) NSMutableArray *productsInWishlist;

-(void)updateProfileWithDictionary:(NSDictionary*)profileDict;
@end
