//
//  UserProfile.m
//  NewCatalog
//
//  Created by Sidney Silva on 05/05/15.
//  Copyright (c) 2015 SYT-101. All rights reserved.
//

#import "UserProfile.h"

@implementation UserProfile

-(void)updateProfileWithDictionary:(NSDictionary*)profileDict{
    self->_name = [profileDict objectForKey:@"name"];
    self->_email = [profileDict objectForKey:@"email"];
    _productsInWishlist = [NSMutableArray arrayWithArray:[profileDict objectForKey:@"wishList"]];
    self->_userId = [profileDict objectForKey:@"userID"];
}
@end
